AbbeDx
## Goblin
* I filen hittad i "./creatures/humanoids/goblin/goblin" står det om en liten sliskig goblin som livnär sig på guld som han stjäler från oskyldiga människor. Goblinen bor i en mörk grotta och lurar in sina fiender med sitt guld. Goblinen har unika attacker där han hoppar på fienden, biter fienden samt lurar fienden med sitt guld och tar den på bar gärning. Goblinen är en undergrupp av humanoids.    

## Ninja
* I filen hittad i "./type/humanoids/ninja" står det om typen ninja, en tyst krigare som inte säger mycket men är skicklig. Ninjan använder sig av shurikens (kastsjärnor) för att döda sina fiender.  
