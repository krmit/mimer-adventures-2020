"use strict";
import {t, Text} from "../../../../lib/text";
import {c, Calculation} from "../../../../lib/calculation";
import Action from "../../../../game/action";
import Creature from "../../creature";
import Humanoid from "../humanoid";

//by abbeXD, a

export default class Golbin extends Humanoid {
	 constructor(name:string, type: string[]) 
	 {
     super(name, ["Goblin"].concat(type));
     this.size = c().value(2).add.dice(0,2);
     this.condition = c().value(15).add.dice(0,5);
     this.strength = c(this).value(5).add.dice(0,10);
     this.flexibility = c().value(10).add.dice(0,5);
     this.intelligent = c().value(0).add.dice(0,3);
     this.charisma = c().value(15).add.dice(0,5);
     this.wisdom = c().value(0).add.dice(0,10);
     this.might = c().value(5).add.dice(0,10);
     this.maxHP = c(this).value(50).add.property("size").add.property("condition");
     this.hp = NaN;
     this.cost += 250;
     
     this._description = t("En sliskig liten varelse som bor i en grotta och gillar att ta guld från oskyldiga människor");
     this._salute = t("Kom in i min lilla grotta så sätter jag en kniv i ditt revben");
     this._ask = t("Vad vill du");
     this._lose = t("Det är fel på dig inte mig");
     this._win = t("HAHAHAHAH");
     this._title = t();
}


Leap(): Action {
   	const my_action = new Action(this, "Hoppar framåt", "påhåppad");
    my_action.tags.push("body");
    my_action.damage.property("flexibility").mult.dice(0,2);
    my_action.initiativ.dice(1,10).add.property("condition")
    my_action.accuracy.value(1).percent.value(60);
    my_action.description = t("Där fick han allt!");
    return my_action;
  }
  
  Bite(): Action {
   const my_action = new Action(this, "Biter fienden", "biten");
   my_action.tags.push("body");
   my_action.damage.property("strength").add.dice(0,5);
   my_action.initiativ.dice(1,10).add.property("condition")
   my_action.accuracy.value(1).percent.value(80);
   my_action.description = t("Det där smakade inte gott!");
   return my_action;
 }

 Charm(): Action {
  const my_action = new Action(this, "Visar fienden sitt guld", "blir lurad");
  my_action.tags.push("body");
  my_action.damage.property("charisma").add.dice(5,25);
  my_action.initiativ.dice(1,10).add.property("condition")
  my_action.accuracy.value(1).percent.value(10);
  my_action.description = t("Där fick du din fattige bonde!");
  return my_action;
}

selection():Action[] {
	 let result = super.selection();
   result.push(this.Leap());
   result.push(this.Bite());
   result.push(this.Charm());
	 this.numberOfSelections+=3;
     return result;
}

static infoChooseMe() {
	 return t("Välj en goblin");
}

static info() {
	 return t("En vanling goblin").blue;
}
}





