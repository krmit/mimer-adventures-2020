"use strict";
import { t, Text } from "@mimer/text";
import { c } from "@mimer/calculation";
import Action from "../../../game/action";
import Creature from "../creature";
import Humanoid from "./humanoid";

export default class EvilKing extends Humanoid {
  static creators = ["Bennymk"];
  static value=350;

  constructor(name: string, type: string[] = [], creators=EvilKing.creators) {
   super(name,["OndKung"].concat(type), creators);
   this.cost += 670; 
   this.size = c().value(7).add.dice(0, 10);
   this.condition = c().value(8).add.dice(0, 10);
   this.strength = c().value(10).add.dice(0, 10);
   this.flexibility = c().value(5).add.dice(0, 12);
   this.intelligent = c().value(10).add.dice(0, 5);
   this.charisma = c().value(13).add.dice(0, 11);
   this.might = c().value(10).add.dice(0, 10);
   this.wisdom = c().value(15).add.dice(0, 15);
   this.maxHP = c(this).value(150).add.property("size").add.property("condition");
  
   this.hp = NaN;
   this._description = t("En helt vanlig mäniska.");
   this._salute = t("Jag kan inte slås!");
   this._lose = t("nej!");
   this._ask = t("Vad ska vi göra?");
   this._win = t("Hur är detta möjligt?");
   this._title = t("OndKung");
  }


  SwordSwing(): Action {
      const my_action = new Action(this, "Ett sving med mäktiga svärdet", "Huggen")

      my_action.tags.push("body");
      my_action.damage.property("strength").add.dice(2,13);
      my_action.initiativ.property("flexibility").mult.value(4)
      my_action.accuracy.value(1).percent.value(89);
      my_action.description= t("Svingar mitt stora, mäktiga svärd!")

return my_action;
  }

 

  selection(): Action[] {
    let result = super.selection();
    result.push(this.SwordSwing());
    this.numberOfSelections++;
    return result;
  }

  damage(action: Action): Action {
    return action;
  }

  static infoChooseMe(): Text {
    return t("Välj en stor stark kung!");
  }

  static info() {
    return t("En ovanligt stark kung!!").blue;
  }
}
