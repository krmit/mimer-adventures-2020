'use strict';
import { t, Text } from '@mimer/text';
import { c } from '@mimer/calculation';
import Action from '../../../game/action';
import Creature from '../creature';
import Humanoid from './humanoid';

/* 
Jag vill skapa en humanoid varelse som blivit biten av en ekorre och därigenom
har fått ekkoregenskaper. Min karaktärs attack(er) ska även vara baserade
på hans ekkoregenskaper.
*/

export default class Squirrelman extends Humanoid{
    static creators = ['Eriu'];
    static value = 275;

    constructor(name: string, type: string[] = [], creators=Squirrelman.creators){
        super(name,['Ekorrmänniska'].concat(type), creators);

        this.cost = this.cost + Squirrelman.value;
        this.size = c().value(8).add.dice(0, 10);
        this.condition = c().value(17).add.dice(0, 10);
        this.strength = c().value(15).add.dice(0, 10);
        this.flexibility = c().value(20).add.dice(0, 10);
        this.intelligent = c().value(10).add.dice(0, 10);
        this.charisma = c().value(15).add.dice(0, 10);
        this.wisdom = c().value(5).add.dice(0, 10);
        this.might = c().value(12).add.dice(0, 10);
        this.maxHP = c(this).value(90).add.property('size').add.property('condition');
        this.hp = NaN;

        this._description = t('En människa som blev biten av en ekorre och fick dens egenskaper.');
        this._salute = t('En ekorre gör allt för sina nötter.');
        this._lose = t('Du var visst en svår nöt att knäcka.');
        this._ask = t('Tänker du låta honom stjäla mina nötter?');
        this._win = t('Trodde du verkligen du hade en chans ditt nöt?');
        this._title = t();
    }

    furiousScratch(): Action{
        const my_action = new Action(this, 'klöser febrilt', 'söndertrasad');
        my_action.tags.push('body');
        my_action.damage.property('strength').add.dice(1, 10);
        my_action.initiativ.dice(1, 10).add.property('condition');
        my_action.accuracy.value(1).percent.value(95);
        my_action.description = t('Flera ordentliga sving med klorna.');
        return my_action;
    }

    selection(): Action[] {
        let result = super.selection();
        result.push(this.furiousScratch());
        this.numberOfSelections++;
        return result;
      }
}