"use strict";
import {t, Text} from "../../../../lib/text";
import {c, Calculation} from "../../../../lib/calculation";
import Action from "../../../../game/action";
import Creature from "../../creature";
import Humanoid from "../humanoid";

//mrE

export default class Dwarf extends Humanoid {
	 constructor(name:string, type: string[]) 
	 {
     super(name, ["Troll"].concat(type));
     this.size = c().value(15).add.dice(0,8);
     this.condition = c().value(5).add.dice(0,10);
     this.strength = c(this).value(30).add.dice(0,30);
     this.flexibility = c().value(2).add.dice(0,3);
     this.intelligent = c().value(4).add.dice(0,5);
     this.charisma = c().value(2).add.dice(0,15);
     this.wisdom = c().value(4).add.dice(0,2);
     this.might = c().value(20).add.dice(0,10);
     this.maxHP = c(this).value(150).add.property("size").add.property("condition");
     this.hp = NaN;
     this.cost += 1000;
     
     this._description = t("En liten varelse som ser ut som en människa och bor i hålor");
     this._salute = t("Jag må vara liten men jag kan slåss");
     this._ask = t("Vad ska jag göra?");
     this._lose = t("Hur kan jag förlora?!?");
     this._win = t("Du skulle inte ha underskattat mig");
     this._title = t();
}


bodyslam(): Action {
   	const my_action = new Action(this, "Kolliderar", "Hårt kolliderad");
    my_action.tags.push("body");
    my_action.damage.property("strength").add.dice(0,10);
    my_action.initiativ.dice(1,10).add.property("condition")
    my_action.accuracy.value(1).percent.value(60);
    my_action.description = t("En riktigt hård kollision!");
    return my_action;
  }
  
selection():Action[] {
	 let result = super.selection();
	 result.push(this.bodyslam());
	 this.numberOfSelections++;
     return result;
}

static infoChooseMe() {
	 return t("Välj ett troll!");
}

static info() {
	 return t("Ett vanligt troll").blue;
}
}




