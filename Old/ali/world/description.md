By Ali

# Varelse

## Guztaf 
En snabb och smidig invandrare med stora och starka armar för att kunna skjuta med sin treudd. Han har stora armar och detta går att hans treuddkast blir så dödligt.
description : "En snabb och smidig invandrare med stora armar"
salute : "Jag ska fighta"
ask : "Hur vill du att jag ska göra?"
lose : "Jag får ändra mitt sätt!"
win : "YAY!"

# Kodbeskrivning:
Filen "guztaf.ts" skapades i "world/creatures/humanoids".
Jag kopierade beskrivningen för Human i "guztaf.ts" och ändrade sedan detta för att det skulle passa just Guztaf. Slutligen importerade jag "guztaf.ts" i "world.ts" filen (rad 13 - "import Guztaf from ./creatures/humanoids/guztaf;") och "selection" metod uppdaterades (rad 29 "Guztaf: Guztaf,")





# undergrupp till “humanoid”

## Vampire 
*En stark varelse som har stora armar och stora ben och är blodsugare. Denna varelse har generellt sett hög "condition" och "strength". Denna varelse attackerar genom att hoppa på fienden, fastna på den och suga blodet. 
description : "En varelse som har stora armar och stora ben och är blodsugare."
salute : "Jag ska kriga!"
ask : "Vad vill du?"
lose : "NEJ!"
win : "Såklart!"

# Kodbeskrivning:
Filen "vampire.ts" skapades i "world/creatures/humanoids/vampires/"
Jag kopierade beskrivningen för dwarf i "vampire.ts" och ändrade sedan detta för att det skulle passa just vampires. Slutligen importerade jag "vampire.ts" i "world.ts" filen (rad 14 - "import Vampire from ./creatures/humanoids/vampires/vampire;") och "selection" metod uppdaterades (rad 30 "Vampire: Vampire,")


## Parazit 
*Parazit är en liten vampire som bor i hålor. Paraziten har unika attacker där den smyger fram till sin fiende och sedan hoppar på den utan att den märker och fastnar på. Den suger fiendens blod väldigt snabbt. Paraziten är liten, tyst, snabb och har hög "flexibility". Den kostar mycket. 
description : "En liten vampire som bor i hålor."
salute : "Liten men stark!"
lose : "Ditt fel!"
win : "Du skulle ha trott på mig!"

# Kodbeskrivning:
Filen "parasite.ts" skapades i "world/creatures/humanoids/vampires/".
Jag kopierade beskrivningen för vampire i "parasite.ts och ändrade sedan detta för att det skulle passa just Parazit. Slutligen importerade jag "parasite.ts" i "world.ts" filen (rad 15 - "import Parasite from ./creatures/humanoids/vampire/parasite;") och "selection" metod uppdaterades (rad 31 "Parasite: Parasite")




# Typ 

## Devil
En demonisk typ som eldar upp sin fiende så att ingen kan stå ut länge. Meningen med denna typ är att ge otroligt ktraftiga och demoniska förmågor och därför garantera att man kommer vinna. 
description :
"En demonisk krigare som eldar upp sin fiende så att ingen kan stå ut länge." ,
"Krigaren har otroligt ktraftiga och demoniska förmågor."
salute : "Ni kommer inte kunna stå ut länge."
lose : "Aldrig"
ask : "Vad önskar du?"
win : "Demonen vinner alltid."
title : "Demonisk Krigare"

# Kodbeskrivning:
Filen "devil.ts" skapades i "world/type/humanoids/"
Jag kopierade beskrivningen för deathKnight i "devil.ts" och ändrade sedan detta för att det skulle passa just devil. Slutligen importerade jag "devil.ts" i "world.ts" filen (rad 16 - "import Devil from ./type/humanoids/devil;") och "selection" metod uppdaterades (rad 38 "devil: Devil")
