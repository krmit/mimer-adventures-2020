
## förslag

För närvarande är det bara möjligt att utföra Action i form av attack som ger skada och därmed minskar HP hos fienden.
Men det bör vara möjligt att även införa en annan typ av Action som skulle vara till att öka HP om den har minskat. 
Denna Action skulle bestämmas av en införd egenkap hos varelserna. 
Tex en egenskap som skulle heta "healing" som är ett mått på hur många gånger som varelsen skulle kunna få öka sin HP.
Denna Action kan skrivas i varelsens attack function som gör att varelsen både attackerar och om den har lägre HP än MaxHP så skulle den även öka HP.
För varje gång HP ökar skulle "healing" egenskapen minska. Om den är noll så kan varelsen inte öka sin HP längre.