"use strict";
import { t, Text } from "@mimer/text";
import { c } from "@mimer/calculation";
import Action from "../../../game/action";
import Humanoid from "../../creatures/humanoids/humanoid";

// By Ali

export default function Devil (base: any) {
  return class Devil extends base {
    static creators = ["Ali"];
    static value = 20000; //mycket högt pris 
    
    constructor(...args: any[]) {
      super(args[0] as string);
      this.cost += Devil.value;
      this.size.mult.value(5);  // i denna typ har "mult" använts vid alla egenskaper. 
      this.condition.mult.value(15);
      this.strength.mult.value(15);
      this.flexibility.mult.value(15);
      this.intelligent.mult.value(15);
      this.charisma.mult.value(15);
      this.wisdom.mult.value(15);
      this.might.mult.value(15);
      this.maxHP = c(this).value(1000).add.property("condition").add.property("strength");
      this._description = t("En demonisk krigare som eldar upp sin fiende så att ingen kan stå ut länge." ,
      "Krigaren har otroligt ktraftiga och demoniska förmågor.");
      this._lose = t("Aldrig");
      this._ask = t("Vad önskar du?");
      this._win = t("Demonen vinner alltid.");
      this._title = t("Demonisk Krigare");
    }

    eld() {
      const my_action = new Action(this, "ELD", "Hårt bränt");
      my_action.tags.push("Fire");
      my_action.damage.dice(1, 20).add.value(20).mult.property("strength");
      my_action.initiativ.property("condition").add.value(10);
      my_action.accuracy.value(1).percent.value(100); //100% nogrannhet.
      my_action.description = t("Eldhav");
      return my_action;
    }

    selection(): Action[] {
      let result = super.selection();
      result.push(this.eld());
      this.numberOfSelections++;
      return result;
    }

    damage(action: Action): Action {
      let result = super.damage(action);
      result.damage.sub.value(70);
      return result;
    }

    static info(): Text {
      return t("En demonisk krigare.").red; //info visas med röd färg när man ska välja.
    }
  };
}
