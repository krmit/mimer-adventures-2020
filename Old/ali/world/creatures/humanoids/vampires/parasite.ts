"use strict";
import {t, Text} from "@mimer/text";
import {c, Calculation} from "@mimer/calculation";
import Action from "../../../../game/action";
import Creature from "../../creature";
import Vampire from "./vampire";

//by Ali 

export default class Parasite extends Vampire {
  static creators = ["Ali"];
  static value=1000;

  constructor(name: string, type: string[], creators=Parasite.creators) {
    super(name, ["Parazit"].concat(type), creators);
    this.cost = this.cost+Parasite.value;

    this.maxHP = c(this).value(220).add.property("condition").add.property("flexibility");
    this.cost += 1000;
    this.size = c().value(5).add.dice(0, 5);  //liten varelse
    this.condition = c().value(25).add.dice(0, 15);
    this.flexibility = c().value(30).add.dice(0, 15);  //högre "flexibility" pga sin storlek.
    this.intelligent = c().value(30).add.dice(0, 10);  //extra duktig
    this.wisdom = c().value(30).add.dice(0, 15);

    this._description = t(
      "En liten vampire som bor i hålor."
    );
    this._salute = t("Liten men stark!");
    this._lose = t("Ditt fel!");
    this._win = t("Du skulle ha trott på mig!");
    this._title = t();
  }

  blodsuga(): Action {
    const my_action = new Action(this, "Suger blod", "Sugit blod");
    my_action.tags.push("body");
    my_action.damage.property("condition").add.dice(0, 15);
    my_action.initiativ.dice(1, 20).add.property("flexibility");
    my_action.accuracy.value(1).percent.value(95);  //högre nogrannhet vid attack jmf med vampire.
    my_action.description = t("En kraftig attack!");
    return my_action;
  }
  static infoChooseMe() {
    return t("Välj lilla Paraziten!").red; //info visas med röd färg.
  }

  static info() {
    return t("En liten stark vampire.").blue;
  }
}