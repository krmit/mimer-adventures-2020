"use strict";
import {t, Text} from "@mimer/text";
import {c, Calculation} from "@mimer/calculation";
import Action from "../../../../game/action";
import Creature from "../../creature";
import Humanoid from "../humanoid";

//by Ali

export default class Vampire extends Humanoid {
  static creators = ["Ali"];
  static value=3000;

  constructor(name: string, type: string[]=[], creators=Vampire.creators) {
    super(name, ["Vampire"].concat(type), creators);
    this.cost = this.cost+Vampire.value;
    this.size = c().value(15).add.dice(0, 10);    
    this.strength = c().value(25).add.dice(0, 10);  //relativt höga egenskaper, härifrån till "might".  
    this.condition = c().value(25).add.dice(0, 10);
    this.flexibility = c().value(20).add.dice(0, 10);
    this.intelligent = c().value(25).add.dice(0, 10);
    this.charisma = c().value(20).add.dice(0, 10);
    this.wisdom = c().value(25).add.dice(0, 10);
    this.might = c().value(25).add.dice(0, 15);
    this.maxHP = c(this).value(200).add.property("condition").add.property("strength");
    this.hp = NaN;
    this.cost += 7000;

    this._description = t(
      "En varelse som har stora armar och stora ben och är blodsugare."
    );
    this._salute = t("Jag ska kriga!");
    this._ask = t("Vad vill du?");
    this._lose = t("NEJ!");
    this._win = t("Såklart!");
    this._title = t();
  }

  blodsuga(): Action {
    const my_action = new Action(this, "Suger blod", "Sugit blod");
    my_action.tags.push("body");
    my_action.damage.property("strength").add.dice(0, 10); //beronde av "strenght"
    my_action.initiativ.dice(1, 10).add.property("condition"); //beroende av "condition".
    my_action.accuracy.value(1).percent.value(90); //hög nogrannhet vid attack.
    my_action.description = t("En rejäl attack!");
    return my_action;
  }

  selection(): Action[] {
    let result = super.selection();
    result.push(this.blodsuga());
    this.numberOfSelections++;
    return result;
  }

  static infoChooseMe() {
    return t("Välj en vampire!").red; //info visas med röd färg när man ska välja.
  }

  static info() {
    return t("En stark vampire.").blue;
  }
}
