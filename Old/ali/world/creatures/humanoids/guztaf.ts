"use strict";
import {t, Text} from "@mimer/text";
import {c, Calculation} from "@mimer/calculation";
import Action from "../../../game/action";
import Creature from "../creature";
import Human from "./human";

//by Ali

export default class Guztaf extends Human {
  static creators = ["Ali"];
  static value=1000; 

  constructor(name: string, type: string[]=[], creators=Guztaf.creators) 
	 {
     super(name, ["Guztaf"].concat(type), creators);
     this.cost = Guztaf.value;
     this.size = c().value(15).add.dice(0,10);   //Ganska stor pga sina stora armar.
     this.strength = c().value(10).add.dice(0,15);  //Hög "strength".
     this.condition = c().value(20).add.dice(0,15); //Hög "condition". 
     this.flexibility = c().value(15).add.dice(0,15); //Hög "strength" + Hög "condition" ger hög "flexibility".
     this.intelligent = c().value(15).add.dice(0,10); 
     this.charisma = c().value(10).add.dice(0,10);
     this.wisdom = c().value(20).add.dice(0,15);
     this.might = c().value(10).add.dice(0,10);
     this.maxHP = c(this).value(100).add.property("size").add.property("condition");
     this.hp = NaN;
     this.cost += 4000; //kostar också ganska mycket.
     
     this._description = t("En snabb och smidig invandrare med stora armar");
     this._salute = t("Jag ska fighta");
     this._ask = t("Hur vill du att jag ska göra?");
     this._lose = t("Jag får ändra mitt sätt!");
     this._win = t("YAY!");
     this._title = t();
}


treudd(): Action {
   	const my_action = new Action(this, "Ett treuddkast", "skjuten");
    my_action.tags.push("body");
    my_action.damage.property("size").add.dice(0,15); //beroende av hans kraftiga armar.
    my_action.initiativ.dice(1,10).add.property("condition") //beroende av hans "condition".
    my_action.accuracy.value(1).percent.value(85);
    my_action.description = t("En dödligt treuddkast");
    return my_action;
  }
  
selection():Action[] {
	 let result = super.selection();
	 result.push(this.treudd());
	 this.numberOfSelections++;
     return result;
}

static infoChooseMe() {
	 return t("Välj magiska Guztaf!").red; //info visas med röd färg när man ska välja.
}

static info() {
	 return t("En människa med kraftiga armar.").blue;
}
}