// by jtm
"use strict";
import { t, Text } from "../../../lib/text";
import { c, Calculation } from "../../../lib/calculation";
import Action from "../../../game/action";
import Creature from "../creature";

export default abstract class Robots extends Creature {
  size!: Calculation;
  condition!: Calculation;
  strength!: Calculation;
  armour!: Calculation;
  //flexability är utbytt mot armour
  intelligent!: Calculation;
  perception!: Calculation;
  //charisma är utbytt mot perception
  wisdom!: Calculation;
  might!: Calculation;
  private _protection = 0;
  private _armor = 0;

  constructor(name: string, type: string[]) {
    super(name, ["humanoid"].concat(type));
  }

  showStats() {
    return t(
      this.showBanner,
      t("Cost        ", String(this.cost), " 💰").setFix(0),
      t("HP          ", String(this.hp), " ❤️").setFix(0),
      t("Max HP      ", this.maxHP.result, " ❤️").setFix(0),
      t("Size        ", this.size.result, " 🔺").setFix(0),
      t("Strength    ", this.strength.result, " 💪").setFix(0),
      t("Condition   ", this.condition.result, " 🏃‍").setFix(0),
      t("armour"      , this.armour.result,    " 🛡️").setFix(0),
      t("perception"  , this.perception.result,  " 👁️").setFix(0),
      t("Intelligent ", this.intelligent.result, " ♟").setFix(0),
      t("Wisdom      ", this.wisdom.result, " 📚").setFix(0),
      t("Might       ", this.might.result, " ❗️").setFix(0),
      t(""), 
      this.labelDescription
    ).list;
  }

  showCalculation() {
    const _this = this;

    return t(
      this.showBanner,
      t("Max HP      ", this.maxHP.toMimerText, " ❤️"),
      t("Size        ", this.size.toMimerText, " 🔺"),
      t("Strength    ", this.strength.toMimerText, " 💪"),
      t("Condition   ", this.condition.toMimerText, " 🏃‍"),
      t("Armour      " , this.armour.toMimerText, " 🛡️"),
      t("Intelligent ", this.intelligent.toMimerText, " ♟"),
      t("Perception  ", this.perception.toMimerText, " 👁️"),
      t("Wisdom      ", this.wisdom.toMimerText, " 📚"),
      t("Might       ", this.might.toMimerText, " ❗️"),
    ).list;
  }

  damage(action: Action): Action {
    return action;
  }

  roll() {
    this.size.roll();
    this.condition.roll();
    this.strength.roll();
    this.armour.roll();
    this.intelligent.roll();
    this.perception.roll();
    this.wisdom.roll();
    this.might.roll();
    this.hp = this.maxHP.result;
  }
}
