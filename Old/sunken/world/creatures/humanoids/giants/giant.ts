"use strict";
import { t, Text } from "@mimer/text";
import { c, Calculation } from "@mimer/calculation";
import Action from "../../../../game/action";
import Creature from "../../creature";
import Humanoid from "../humanoid";


export default class Giant extends Humanoid {
  static creators = ["SunKen"];
  static value=600;

  constructor(name: string, type: string[]=[], creators=Giant.creators) {
    super(name, ["Jätte"].concat(type), creators);
    this.cost = this.cost+Giant.value;
    this.size = c()
      .value(10)
      .add.dice(2, 8);
    this.condition = c()
      .value(5)
      .add.dice(0, 10);
    this.strength = c(this)
      .value(9)
      .add.dice(0, 7);
    this.flexibility = c()
      .value(3)
      .add.dice(0, 14);
    this.intelligent = c()
      .value(1)
      .add.dice(0, 15)
    this.charisma = c()
      .value(5)
      .add.dice(0, 10);
    this.wisdom = c()
      .value(1)
      .add.dice(1, 10);
    this.might = c()
      .value(5)
      .add.dice(0, 12);
    this.maxHP = c(this)
      .value(102)
      .add.property("size")
      .add.property("condition");
    this.hp = NaN;
    this.cost += 600;

    this._description = t(
      "En stor människoliknande varelse som bor i djupa grottor"
    );
    this._salute = t("Lika stor och stark som en borg");
    this._ask = t("Vad ska jag göra??");
    this._lose = t("Hur är detta möjlig att någon lyckats klå mig?!?");
    this._win = t("Styrka vinner alltid över allt annat!");
    this._title = t();
  }

  heavyKick(): Action {
    const my_action = new Action(this, "Sparka Kraftigt", "Kraftigt Sparkad");
    my_action.tags.push("body");
    my_action.damage.property("strength").add.dice(4, 10);
    my_action.initiativ.dice(4, 10).add.property("condition");
    my_action.accuracy.value(1).percent.value(50);
    my_action.description = t("En mycket kraftfull och brutal spark!");
    return my_action;
  }

  selection(): Action[] {
    let result = super.selection();
    result.push(this.heavyKick());
    this.numberOfSelections++;
    return result;
  }

  static infoChooseMe() {
    return t("Välj en Jätte");
  }

  static info() {
    return t("En Jätte").blue;
  }
}
