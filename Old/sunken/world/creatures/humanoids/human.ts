"use strict";
import { t, Text } from "@mimer/text";
import { c } from "@mimer/calculation";
import Action from "../../../game/action";
import Creature from "../creature";
import Humanoid from "./humanoid";

export default class Human extends Humanoid {
  static creators = ["krm"];
  static value=250;

  constructor(name: string, type: string[] = [], creators=Human.creators) {
    super(name,["Mäniska"].concat(type), creators);
    this.cost = this.cost+Human.value;
    this.size = c()
      .value(10)
      .add.dice(0, 10);
    this.condition = c()
      .value(10)
      .add.dice(0, 10);
    this.strength = c()
      .value(10)
      .add.dice(0, 10);
    this.flexibility = c()
      .value(10)
      .add.dice(0, 10);
    this.intelligent = c()
      .value(10)
      .add.dice(0, 10);
    this.charisma = c()
      .value(10)
      .add.dice(0, 10);
    this.wisdom = c()
      .value(10)
      .add.dice(0, 10);
    this.might = c()
      .value(10)
      .add.dice(0, 10);
    this.maxHP = c(this)
      .value(50)
      .add.property("size")
      .add.property("condition");
    this.hp = NaN;
    this._description = t("En helt vanlig mäniska.");
    this._salute = t("Jag kan inte slås!");
    this._lose = t("nej!");
    this._ask = t("Vad ska vi göra?");
    this._win = t("Hur är detta möjligt?");
    this._title = t();
  }

  hit(): Action {
    const my_action = new Action(this, "Ett knytnävesslag", "slagen");
    my_action.tags.push("body");
    my_action.damage.dice(1, 10).add.property("strength");
    my_action.initiativ.property("flexibility").mult.value(2);
    my_action.accuracy.value(1).percent.value(80);
    my_action.description = t(
      "Ett knytnävesslag, ger inte mycket skada men går snabbt"
    );
    return my_action;
  }

  selection(): Action[] {
    let result = super.selection();
    result.push(this.hit());
    this.numberOfSelections++;
    return result;
  }

  damage(action: Action): Action {
    return action;
  }

  static infoChooseMe(): Text {
    return t("Välj en vanlig mäniska!").red;
  }

  static info() {
    return t("En vanlig mäniska!").blue;
  }
}
