"use strict";
import Creature from "./creatures/creature";
import Humanoid from "./creatures/humanoids/humanoid";
import Lizardman from "./creatures/humanoids/lizardmen/lizardman";
import Dwarf from "./creatures/humanoids/dwarf/dwarf";
import Longbeards from "./creatures/humanoids/dwarf/longbeards";
import Human from "./creatures/humanoids/human";
import Knight from "./type/humanoids/knight";
import Strong from "./type/humanoids/strong";
import Weak from "./type/humanoids/weak";
import DeathKnight from "./type/humanoids/deathKnight";
import Giant from "./creatures/humanoids/giants/giant";

interface worldInterface {
  creatures: {[key: string]: any},
  types: {[key: string]: any} 
}

let world: worldInterface = {
  creatures: {
    Human: Human,
    Lizardman: Lizardman,
    Dwarf: Dwarf,
    Longbeards: Longbeards,
    Giant: Giant
  },
  types: {
    knight: Knight,
    weak: Weak,
    strong: Strong,
    deadKnight: DeathKnight
  }
};

export default world;
