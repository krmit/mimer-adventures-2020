"use strict";
import { t, Text, TextToString } from "@mimer/text";

/**
 * The Calculation class represent calculation.
 */
export default abstract class Resource {
  static creators = ["krm"];
  static value=0;
  name:string;
  cost:number;
  creators:string[];


  constructor(name: string, creators=Resource.creators) {
    this.name = name;
    this.cost = Resource.value;
    this.creators = creators;
  }
}
  
