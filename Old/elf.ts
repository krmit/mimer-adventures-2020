"use strict";
import {t, Text} from "../../../lib/text";
import {c, Calculation} from "../../../lib/calculation";
import Action from "../../../game/action";
import Creature from "../creature";
import Humanoid from "./humanoid";

//by epidem

export default class Elf extends Humanoid {
	 constructor(name:string, type: string[]) 
	 {
     super(name, ["Alv"].concat(type));
     this.size = c().value(13).add.dice(0,10);
     this.condition = c().value(12).add.dice(0,5);
     this.strength = c(this).value(5).add.dice(0,5);
     this.flexibility = c().value(8).add.dice(0,10);
     this.intelligent = c().value(15).add.dice(0,15);
     this.charisma = c().value(7).add.dice(0,10);
     this.wisdom = c().value(15).add.dice(0,15);
     this.might = c().value(5).add.dice(0,5);
     this.maxHP = c(this).value(90).add.property("size").add.property("condition");
     this.hp = NaN;
     this.cost += 700;
     
     this._description = t("En oftast långsmal, smidig varelse med generellt sätt hög intelligens");
     this._salute = t("Jag skall strida");
     this._ask = t("Vad krävs?");
     this._lose = t("Inte så här");
     this._win = t("Seger är för evigt");
     this._title = t();
}


fireball(): Action {
   	const my_action = new Action(this, "Ett eldbollskast", "skjuten");
    my_action.tags.push("body");
    my_action.damage.property("intelligent").add.dice(0,10);
    my_action.initiativ.dice(1,10).add.property("intelligent")
    my_action.accuracy.value(1).percent.value(80);
    my_action.description = t("En magisk eldboll");
    return my_action;
  }
  
selection():Action[] {
	 let result = super.selection();
	 result.push(this.fireball());
	 this.numberOfSelections++;
     return result;
}

static infoChooseMe() {
	 return t("Välj en alv");
}

static info() {
	 return t("En vanlig alv").blue;
}
}
