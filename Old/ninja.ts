"use strict";
import { t, Text } from "../../../lib/text";
import { c } from "../../../lib/calculation";
import Action from "../../../game/action";
import Humanoid from "../../creatures/humanoids/humanoid";

export default function Ninja(base: any) {
  return class extends base {
    constructor(...args: any[]) {
      super(args[0] as string);
      this.cost += 600;
      this._description = t(
        "Ninjan är en tyst krigare."
      );
      this._salute = t("...");
      this._lose = t("...");
      this._win = t("...");
      this._title = t("Ninja");
    }

    Shuriken() {
      const my_action = new Action(this, "Shuriken", "träffad av shuriken");
      my_action.tags.push("steel");
      my_action.damage.dice(0, 3).mult.property("strength");
      my_action.initiativ.property("flexibility").add.value(2);
      my_action.accuracy.value(1).percent.value(50);
      my_action.description = t("En kaststjärna");
      return my_action;
    }

    selection(): Action[] {
      let result = super.selection();
      result.push(this.cut());
      this.numberOfSelections++;
      return result;
    }

    damage(action: Action): Action {
      let result = super.damage(action);
      result.damage.sub.value(10);
      return result;
    }

    static info(): Text {
      return t("En bra ninja!").blue;
    }
  };
}
