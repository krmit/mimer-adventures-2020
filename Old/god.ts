"use strict";
import { t, Text } from "../../../lib/text";
import { c } from "../../../lib/calculation";
import Action from "../../../game/action";
import Humanoid from "../../creatures/humanoids/humanoid";

// epidem

export default function God(base: any) {
  return class extends base {
    constructor(...args: any[]) {
      super(args[0] as string);
      this.size.div.value(100);
      this.condition.div.value(100);
      this.strength.div.value(100);
      this.flexibility.div.value(100);
      this.intelligent.div.value(100);
      this.charisma.div.value(100);
      this.wisdom.div.value(100);
      this.might.div.value(100);
      this.maxHP = c(this).value(1000);

      this.cost += 10000;
      this._description = t(
        "En status som ger gudalika förmågor"
      );
      this._salute = t("Ni är alla under mig!");
      this._lose = t("Detta är inte möjligt!");
      this._ask = t("Vad kräver du?");
      this._win = t("Det var förutbestämt");
      this._title = t("Gud");
    }

    smite(): Action {
      const my_action = new Action(this, "Spränger", "bler sprängd");
      my_action.tags.push("body");
      my_action.damage.property("intelligent").mult.dice(1,10);
      my_action.initiativ.property("intelligent");
      my_action.accuracy.value(1).percent.value(100);
      my_action.description = t("En gudalik attack");
      return my_action;
    }

    selection(): Action[] {
      let result = super.selection();
      result.push(this.smite());
      this.numberOfSelections++;
      return result;
    }

    static info(): Text {
      return t("En gudalik varelse.").blue;
    }
  };
}
