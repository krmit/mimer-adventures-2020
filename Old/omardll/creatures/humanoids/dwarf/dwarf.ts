"use strict";
import { t, Text } from "@mimer/text";
import { c, Calculation } from "@mimer/calculation";
import Action from "../../../../game/action";
import Creature from "../../creature";
import Humanoid from "../humanoid";

//by krm, a

export default class Dwarf extends Humanoid {
  static creators = ["krm"];
  static value=600;

  constructor(name: string, type: string[]=[], creators=Dwarf.creators) {
    super(name, ["Dvärg"].concat(type), creators);
    this.cost = this.cost+Dwarf.value;
    this.size = c()
      .value(4)
      .add.dice(0, 8);
    this.condition = c()
      .value(10)
      .add.dice(0, 10);
    this.strength = c(this)
      .value(10)
      .add.dice(0, 15);
    this.flexibility = c()
      .value(5)
      .add.dice(0, 10);
    this.intelligent = c()
      .value(10)
      .add.dice(0, 10);
    this.charisma = c()
      .value(5)
      .add.dice(0, 15);
    this.wisdom = c()
      .value(12)
      .add.dice(0, 10);
    this.might = c()
      .value(10)
      .add.dice(0, 10);
    this.maxHP = c(this)
      .value(85)
      .add.property("size")
      .add.property("condition");
    this.hp = NaN;
    this.cost += 600;

    this._description = t(
      "En liten varelse som ser ut som en människa och bor i hålor"
    );
    this._salute = t("Jag må vara liten men jag kan slåss");
    this._ask = t("Vad ska jag göra?");
    this._lose = t("Hur kan jag förlora?!?");
    this._win = t("Du skulle inte ha underskattat mig");
    this._title = t();
  }

  hardHit(): Action {
    const my_action = new Action(this, "Slår hårt", "hårt slagen");
    my_action.tags.push("body");
    my_action.damage.property("strength").add.dice(0, 10);
    my_action.initiativ.dice(1, 10).add.property("condition");
    my_action.accuracy.value(1).percent.value(70);
    my_action.description = t("Ett riktigt hårt slag!");
    return my_action;
  }

  selection(): Action[] {
    let result = super.selection();
    result.push(this.hardHit());
    this.numberOfSelections++;
    return result;
  }

  static infoChooseMe() {
    return t("Välj en dvärg");
  }

  static info() {
    return t("En vanlig Dvärg").blue;
  }
}
