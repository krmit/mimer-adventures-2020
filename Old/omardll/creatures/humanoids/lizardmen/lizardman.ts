"use strict";
import { t, Text } from "@mimer/text";
import { c, Calculation } from "@mimer/calculation";
import Action from "../../../../game/action";
import Creature from "../../creature";
import Humanoid from "../humanoid";

//by krm, pleun

export default class Lizardman extends Humanoid {
  static creators = ["pleun"];
  static value=200;

  constructor(name: string, type: string[], creators=Lizardman.creators) {
    super(name, ["Ödleman"].concat(type), creators);

    this.size = c()
      .value(15)
      .add.dice(0, 20);
    this.condition = c()
      .value(10)
      .add.dice(0, 10);
    this.strength = c(this)
      .property("size")
      .add.dice(0, 10);
    this.flexibility = c()
      .value(10)
      .add.dice(0, 10);
    this.intelligent = c()
      .value(5)
      .add.dice(0, 5);
    this.charisma = c()
      .value(0)
      .add.dice(0, 5);
    this.wisdom = c()
      .value(0)
      .add.dice(0, 10);
    this.might = c()
      .value(20)
      .add.dice(0, 20);
    this.maxHP = c(this)
      .value(75)
      .add.property("size")
      .add.property("condition");
    this.hp = NaN;
    this.cost += 500;

    this._description = t("En liten ödla");
    this._salute = t("Du kommer aldrig kunna besegra detta hämska monster!");
    this._ask = t("Hur ska vi göra nu då?");
    this._lose = t("Heck!");
    this._win = t("Rekt, gitgud");
    this._title = t();
  }

  damage(action: Action): Action {
    return action;
  }

  Scratch(): Action {
    const my_action = new Action(this, "Riva", "riven med klor");
    my_action.tags.push("body");
    my_action.damage.value(12);
    my_action.initiativ.dice(1, 10).add.property("condition");
    my_action.accuracy.value(1).percent.value(80); //0.7 + this.flexibility / 20;
    my_action.description = t("Känn kraften av min programerings kunskap!");
    return my_action;
  }

  Shank(): Action {
    const my_action = new Action(this, "Svans", "genomborade med sin svans");
    my_action.tags.push("steel");
    my_action.damage
      .dice(1, 10)
      .add.property("strength")
      .add.property("size");
    my_action.initiativ.dice(1, 10).add.property("condition");
    my_action.accuracy.value(1).percent.value(80); //this.flexibility / (this.flexibility*(this.strength *0.5));
    my_action.description = t("Pleun Saarloos sends his regards!");
    return my_action;
  }

  UnstablefireBall(): Action {
    const my_action = new Action(
      this,
      "Ostabil eldboll",
      "sprutade en eldboll"
    );
    my_action.tags.push("steel");
    my_action.damage.dice(1, 10).mult.property("might");
    my_action.initiativ.dice(1, 5).add.property("condition");
    my_action.accuracy.value(1).percent.value(20); //this.flexibility / (this.flexibility*(this.strength *0.5));
    my_action.description = t("Detta kommer göra ont");
    return my_action;
  }

  selection(): Action[] {
    let result = super.selection();
    result.push(this.Scratch());
    result.push(this.Shank());
    result.push(this.UnstablefireBall());
    this.numberOfSelections += 3;
    return result;
  }

  static infoChooseMe() {
    return t("En kobold ödla");
  }

  static info() {
    return t("En kobold ödla").blue;
  }
}
