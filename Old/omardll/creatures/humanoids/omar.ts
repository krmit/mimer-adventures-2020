"use strict";
import { t, Text } from "@mimer/text";
import { c } from "@mimer/calculation";
import Action from "../../../game/action";
import Creature from "../creature";
import Humanoid from "./humanoid";

export default class Omardll extends Humanoid{
  static creators = ["omardll"];
  static value=450;

  constructor(name: string, type: string[] = [], creators=Omardll.creators) {
    super(name,["Mäniska-1"].concat(type), creators);
    this.cost = this.cost+Omardll.value;
    this.size = c()
      .value(10)
      .add.dice(0, 15);
    this.condition = c()
      .value(20)
      .add.dice(0, 15);
    this.strength = c()
      .value(45)
      .add.dice(0, 5);
    this.flexibility = c()
      .value(30)
      .add.dice(0, 5);
    this.intelligent = c()
      .value(10)
      .add.dice(0, 10);
    this.charisma = c()
      .value(15)
      .add.dice(0, 15);
    this.wisdom = c()
      .value(20)
      .add.dice(0, 15);
    this.might = c()
      .value(10)
      .add.dice(0, 10);
    this.maxHP = c(this)
      .value(75)
      .add.property("size")
      .add.property("condition");
    this.hp = NaN;
    this._description = t("En boxare.");
    this._salute = t("Jag kommer slå sönder dig!");
    this._lose = t("Aj, du verkar vara stark!");
    this._ask = t("Vad ska vi göra?");
    this._win = t("Haha du behöver mer träning!");
    this._title = t("Boxare");
  }

  punsh(): Action {
    const my_action = new Action(this, "En uppercut!", "slagen");
    my_action.tags.push("body");
    my_action.damage.dice(1, 20).add.property("strength");
    my_action.initiativ.property("flexibility").mult.value(5);
    my_action.accuracy.value(5).percent.value(75);
    my_action.description = t(
      "En uppercut gör ganska mycket skada, och den är väldigt snabb."
    );
    return my_action;
  }

  selection(): Action[] {
    let result = super.selection();
    result.push(this.punsh());
    this.numberOfSelections++;
    return result;
  }

  damage(action: Action): Action {
    return action;
  }

  static infoChooseMe(): Text {
    return t("Välj en boxare!").green;
  }

  static info() {
    return t("En boxare!").blue;
  }
}
