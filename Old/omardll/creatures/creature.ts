"use strict";
import { t, Text, TextToString } from "@mimer/text";
import { c, Calculation } from "@mimer/calculation";
import Action from "../../game/action";
import Resource from "../resource";

/**
 * The Calculation class represent calculation.
 */
export default abstract class Creature extends Resource {
  static creators = ["krm"];
  static value=0;
  hp = 0;
  maxHP!: Calculation;
  type: string[];
  numberOfSelections = 0;
  protected _title!: Text;
  protected _description!: Text;
  protected _salute!: Text;
  protected _ask!: Text;
  protected _lose!: Text;
  protected _win!: Text;

  constructor(name: string, type: string[], creators=Creature.creators) {
    super(name, creators);
    this.cost = this.cost+Creature.value;
    this.type = type;
  }

  get labelName(): Text {
    if (TextToString(this._title) !== "") {
      return t(TextToString(this._title), " ", this.name).green; // Bad fix of bug.
    } else {
      return t(this.name).green;
    }
  }

  get labelType(): Text {
    return t(this.type.join("/"));
  }

  get labelAuthors(): Text {
    return t("Skapat av ", this.creators.join(", "));
  }

  get labelDescription(): Text {
    return this._description;
  }

  get sayName(): Text {
    return t(this.labelName, t(": ").bold);
  }

  get saySalute(): Text {
    return t(this.sayName, this._salute);
  }

  get sayQuestion(): Text {
    return t(this.sayName, this._ask);
  }

  get sayWin(): Text {
    return t(this.sayName, this._win);
  }

  get sayLose(): Text {
    return t(this.sayName, this._lose);
  }

  get showTitel(): Text {
    return this.labelName.headline;
  }

  get showBanner(): Text {
    return t(this.showTitel, "\n", this.labelType, t().newline, "\n", this.labelAuthors)
      .bold.newline;
  }

  get showStatus(): Text {
    if (this.isFighting()) {
      return t(
        this.sayName,
        t(String(this.hp)).bold,
        "/",
        String(this.maxHP.result),
        " ❤️"
      );
    } else {
      return t(this.sayName, " 🙁 ");
    }
  }

  static showChooseMe(): Text {
    return t("No info");
  }

  static createRandom(): any {
    return null;
  }

  isFighting(): boolean {
    return this.hp > 0;
  }

  selection(): Action[] {
    this.numberOfSelections = 0;
    return [];
  }

  do(action: Action): Text {
    let roll =
      c()
        .dice(1, 100)
        .roll().result / 100.0;
    action.roll();
    let result = t();
    if (roll < action.accuracy.result) {
      result
        .add(action.showDamageLabel)
        .add(
          t(
            "Aj, jag tog ",
            String(action.damage.result),
            " ❤️  i skada. Chansen var ",
            t(String(action.accuracy.result)).percent,
            " 🎯"
          ).newline
        );
      this.hp -= action.damage.result;
      if (!this.isFighting()) {
        result.add(this.sayLose.newline);
        result.add(action.by.sayWin.newline);
      }
    } else {
      result
        .add(action.showDamageLabel)
        .add("Ha! Du missade! Chansen var ")
        .add(t(String(action.accuracy.result)).percent)
        .add(" 🎯").newline;
    }
    return result;
  }

  abstract showStats(): Text;

  abstract damage(action: Action): Action;

  abstract  roll(action: void): void;
}
