# epidem
## Del 1

# E - Nivå

	Jag har valt att skapa alver. Alver är ett magiskt folk som oftast lever i skogsområden, långt bort ifrån mänsklig civilisation. Deras livslängd är ofta betydligt mycket längre än en vanlig människas, och är ofta hyfsat smala och långa.

	Skapade filen "elf.ts" under world/creatures/humanoids
	Ändrade i "world.ts" filen för att göra alver vara valbara

# C - Nivå

	Jag har valt att skapa en ny typ vid namned "god". Meningen med denna typ är att ge otroligt starka förmågor till en varelse, men kostnaden för att få typen är extremt hög (10000). Den enda typen som verkar kunna besegra God är Deathknight. 

	Skapade filen "god.ts" under world/type/humanoids
	Ändrare i "world.ts" filen för att tillåta att varelser har typen God

# A - Nivå

	Jag har valt att skapa goblins. Goblins är korta varelser med färgglatt skinn, som oftast är rätt så oempatiska och grymma. De är giriga varelser som inte går att lita på.

## Del 2

# A - Nivå

	Jag har valt att implementera en ny basklass. Basklassen kalla för "undead" och finns att hitta under world/creatures/undeads. Klassen delar mycket med humanoids, men har inte tillgång till charisma, intelligent, och wisdom. Detta är av anledningen att undeads ska klasses som hjärndöda, därav har de inte tillgång till stats som kräver tänkande (intelligent och wisdom). De har inte heller tillgång till charisma, då de är just undead.

	Undeads har istället tillgång till decay, regeneration och aggression. Decay är menat att visa hur förmultna undeads är. En undead som nytt har dött har en låg decay stat, medan en undead som har levt under lång tid har en hög decay stat. En zombie exempelvis skulle ha lägre decay stat än ett skeleton. Regeneration är menat att visa om varelsen har någon form av regeneration eller inte. Sist men inte minst visar aggression om varelsen är aggressiv, och i så fall hur aggressin varelsen är. Undeads med relativt låg aggression kanske kan lämnas ifred, medan undeads med hög aggression kommer attackera allt och alla blint.  
