"use strict";
import {t, Text} from "./../../../lib/text";
import {c, Calculation} from "./../../../lib/calculation";
import Action from "./../../../game/action";
import Creature from "./../creature";
import Humanoid from "./humanoid";


export default class Santa extends Humanoid {
	 constructor(name:string, type: string[]) 
	 {
     super(name, ["Tomte"].concat(type));
     this.size = c().value(8).add.dice(3, 12);
     this.condition = c().value(1).add.dice(0,10);
     this.strength = c(this).value(10).add.dice(0,15);
     this.flexibility = c().value(1).add.dice(1,3);
     this.intelligent = c().value(10).add.dice(5,10);
     this.charisma = c().value(2).add.dice(1,8);
     this.wisdom = c().value(10).add.dice(5,10);
     this.might = c().value(7).add.dice(1,10);
     this.maxHP = c(this).value(100).add.property("size").add.property("condition");
     this.hp = NaN;
     this.cost += 800;
     
     this._description = t("Snubben som tömmer säcken för barn");
     this._salute = t("Jag kan inte se läskig ut men jag leverarar inte presenter till dig");
     this._ask = t("Hur mycket presenter ska jag ge?");
     this._lose = t("Du får ändå inga presenter!!");
     this._win = t("Inga presenter för dig, hahaha");
     this._title = t();
}


mindrePresenter(): Action {
   	const my_action = new Action(this, "Bränner presenterna", "Presenterna borta");
    my_action.tags.push("body");
    my_action.damage.property("strength").add.dice(0,10);
    my_action.initiativ.dice(1,10).add.property("condition")
    my_action.accuracy.value(1).percent.value(70);
    my_action.description = t("Bränner upp presenterna framför fienden");
    return my_action;
  }
  
selection():Action[] {
	 let result = super.selection();
	 result.push(this.mindrePresenter());
	 this.numberOfSelections++;
     return result;
}

static infoChooseMe() {
	 return t("Välj tomten");
}

static info() {
	 return t("En tjock tomte").blue;
}
}





