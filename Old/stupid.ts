"use strict";
import { t, Text } from "../../../lib/text";
import { c } from "../../../lib/calculation";
import Action from "../../../game/action";
import Humanoid from "../../creatures/humanoids/humanoid";

export default function Stupid(base: any) {
  return class extends base {
    constructor(...args: any[]) {
      super(args[0] as string);
      this.cost *= 0.5;
      this.size.mult.value(3);
      this.condition.mult.value(2);
      this.strength.mult.value(3);
      this.flexibility.mult.value(2);
      this.intelligent.div.value(10);
      this.charisma.div.value(2);
      this.wisdom.div.value(4);
      this.might.mult.value(0);
      this.maxHP = c(this)
        .value(50)
        .add.property("size")
        .add.property("condition");
    }

    static info(): Text {
      return t("En stark men korkad krigare!").blue;
    }
  };
}
