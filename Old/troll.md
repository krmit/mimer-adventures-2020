mrE

###Del I Göra en karaktär

##E-Nivå

#Jag vill skapa en hobbit. Hobbitarna ska vara små men snabba och luriga. Deras unika attack är ett hugg med hög precision som baseras på deras flexibilitet.

#Därmed skapade jag en fil "hobbit.ts" i world/creatures/humanoids. Jag kopierade beskrivningen för dwarfs och ändrade sedan detta för att det skulle passa just hobbitar. Slutligen importerade jag hobbits i world.ts filen.

##C-Nivå

#Jag har gjort en typ "wizard" som är väldigt stark men kostar också väldigt mycket mer. Trollkarlar attackerar med en magisk formel, så kallad "spell". Om man inte tar hänsyn till kostnaden så är denna typ klart dominerande.

##A-Nivå

#Härnäst vill jag skapa troll. Troll ska vara stora varelser med högt hp och hög styrka. Deras unika attack är en bodyslam som skadar mycket men inte har jättehög träffsäkerhet.

#Denna humanoid skapade i en mapp "troll" som ligger under world/creatures/humanoids.

###Del II

##A-Nivå

#Jag lade till en ny basklass, så kallad "Slimes" och den finns lokaliserad under World/creatures. Det som skiljer slimes från Humanoider är att de sakna intelligent, charisma samt wisdom. Istället har de blivit tilldelade två nya egenskaper, bounciness samt lifesteal. Jag anser att dessa egenskaper passar slime bättre då de saknar förmågan att tänka.


