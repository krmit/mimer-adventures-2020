"use strict";
import { t, Text } from "../../../lib/text";
import { c, Calculation } from "../../../lib/calculation";
import Action from "../../../game/action";
import Creature from "../creature";

export default abstract class Undead extends Creature {
  size!: Calculation;
  condition!: Calculation;
  strength!: Calculation;
  flexibility!: Calculation;
  might!: Calculation;
  decay!: Calculation;
  regeneration!: Calculation;
  aggression!: Calculation;
  private _protection = 0;
  private _armor = 0;

  constructor(name: string, type: string[]) {
    super(name, ["undead"].concat(type));
  }

  showStats() {
    return t(
      this.showBanner,
      t("Cost         ", String(this.cost), " 💰").setFix(0),
      t("HP           ", String(this.hp), " ❤️").setFix(0),
      t("Max HP       ", this.maxHP.result, " ❤️").setFix(0),
      t("Size         ", this.size.result, " 🔺").setFix(0),
      t("Strength     ", this.strength.result, " 💪").setFix(0),
      t("Condition    ", this.condition.result, " 🏃‍").setFix(0),
      t("Flexibility  ", this.flexibility.result, " 💃").setFix(0),
      t("Might        ", this.might.result, " ❗️").setFix(0),
      t("Decay        ", this.decay.result, " 💀").setFix(0),
      t("Regeneration ", this.regeneration.result, " ✨").setFix(0),
      t("Aggression   ", this.aggression.result, " 😡").setFix(0),
      t(""), 
      this.labelDescription
    ).list;
  }

  showCalculation() {
    const _this = this;

    return t(
      this.showBanner,
      t("Max HP       ", this.maxHP.toMimerText, " ❤️"),
      t("Size         ", this.size.toMimerText, " 🔺"),
      t("Strength     ", this.strength.toMimerText, " 💪"),
      t("Condition    ", this.condition.toMimerText, " 🏃‍"),
      t("Flexibility  ", this.flexibility.toMimerText, " 💃"),
      t("Might        ", this.might.toMimerText, " ❗️"),
      t("Decay        ", this.decay.toMimerText, " 💀"),
      t("Regeneration ", this.regeneration.toMimerText, " ✨"),
      t("Aggression   ", this.aggression.toMimerText, " 😡"),
    ).list;
  }

  damage(action: Action): Action {
    return action;
  }

  roll() {
    this.size.roll();
    this.condition.roll();
    this.strength.roll();
    this.flexibility.roll();
    this.might.roll();
    this.decay.roll();
    this.regeneration.roll();
    this.aggression.roll();
    this.hp = this.maxHP.result;
  }
}
