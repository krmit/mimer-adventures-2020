"use strict";
import { t, Text } from "@mimer/text";
import { c } from "@mimer/calculation";
import Action from "../../../game/action";
import Humanoid from "../../creatures/humanoids/humanoid";

export default function DrunkenRage(base: any) {
  return class DrunkenRage extends base {
    static creators = ["BigBoySteven"];
    static value = 300;

    constructor(...args: any[]) {
      super(args[0] as string);
      this.cost *= 1.25; //karaktärens cost ökar med 25%
      this.size.add.value(5);
      this.condition.add.value(-5);
      this.strength.add.value(10);
      this.flexibility.add.value(-3);
      this.intelligent.div.value(2);
      this.charisma.add.value(4);
      this.wisdom.div.value(2);
      this.might.mult.value(1.5); //karaktärens might ökar med 50%
      this.maxHP = c(this)
        .value(50)
        .add.property("size")
        .add.property("condition");
    }

    static info(): Text {
      return t("Ett argt fyllo").blue;
    }
  };
}
