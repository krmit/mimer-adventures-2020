"use strict";
import { t, Text } from "@mimer/text";
import { c, Calculation } from "@mimer/calculation";
import Action from "../../../../game/action";
import Creature from "../../creature";
import Humanoid from "../humanoid";

//by krm, BigBoySteven

export default class Archer extends Humanoid {
  static creators = ["BigBoySteven"];
  static value=400;

  constructor(name: string, type: string[], creators=Archer.creators) {
    super(name, ["Bågskytt"].concat(type), creators);

    this.size = c()
      .value(10)
      .add.dice(0, 10);
    this.condition = c()
      .value(20)
      .add.dice(0, 10);
    this.strength = c(this)
      .property("size")
      .add.dice(0, 7);
    this.flexibility = c()
      .value(15)
      .add.dice(0, 10);
    this.intelligent = c()
      .value(20)
      .add.dice(0, 5);
    this.charisma = c()
      .value(15)
      .add.dice(0, 10);
    this.wisdom = c()
      .value(20)
      .add.dice(0, 15);
    this.might = c()
      .value(10)
      .add.dice(0, 10);
    this.maxHP = c(this)
      .value(40) //base value på maxHP sätts till 40 och ökar beroende på size och condition
      .add.property("size")
      .add.property("condition");
    this.hp = NaN;
    this.cost += 400;

    this._description = t("En skicklig bågskytt");
    this._salute = t("Jag är oöverträffad med min pilbåge!");
    this._ask = t("Hur ska jag besegra fienden?");
    this._lose = t("NEJ! Detta är omöjligt");
    this._win = t("Jag skulle ljuga om jag sa att jag var förvånad");
    this._title = t();
  }

  damage(action: Action): Action {
    return action;
  }

  vanligPil(): Action { //vanlig pil som gör lite damage men träffar nästan alltid
    const my_action = new Action(this, "Vanlig pil", "Skjuter med en vanlig pil");
    my_action.tags.push("body");
    my_action.damage.value(20);
    my_action.initiativ.dice(1, 10).add.property("condition");
    my_action.accuracy.value(1).percent.value(90);
    my_action.description = t("En vanlig pil bör vara tillräckligt för någon som dig");
    return my_action;
  }

  eldPil(): Action { //En brinnande pil som gör mer damage än vanliga pilen med minskad accuracy(gör mer damage men träffar inte like ofta)
    const my_action = new Action(this, "Eld pil", "Skjuter en brinnande pil!");
    my_action.tags.push("fire");
    my_action.damage.value(20).add.property("wisdom");
    my_action.initiativ.dice(1, 10).add.property("condition");
    my_action.accuracy.value(1).percent.value(75);
    my_action.description = t("Elden renar världen från det onda!");
    return my_action;
  }

  Boost(): Action { //Stat booster som ger ökad strength och might, men minskad intelligent
    const my_action = new Action(this, "Boost", "Dricker en energi dricka");
    this.strength = c(this)
    .add.value(15);
    this.condition = c()
    .add.value(10);
    this.might = c()
    .add.value(20);
    this.intelligent = c()
    .add.value(20);
    my_action.description = t("Jag känner mig ostoppbar!!!");
    return my_action;
  }

  selection(): Action[] {
    let result = super.selection();
    result.push(this.vanligPil());
    result.push(this.eldPil());
    result.push(this.Boost());
    this.numberOfSelections += 3;
    return result;
  }

  static infoChooseMe() {
    return t("Välj en skicklig bågskytt");
  }

  static info() {
    return t("En skicklig bågskytt").blue;
  }
}
