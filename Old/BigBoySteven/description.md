BigBoySteven

## Archer
Jag gjorde en creature som är en bågskytt och kan hittas i "./creatures/humanoids/mankind/archer.ts". Det är en intelligent och skicklig bågskytt med lite hp.

## drunkenRage
Jag gjorde en type som alla karaktärer kan använda och hittas i "./type/humanoids/drunkenRage.ts" och gör karaktären till ett fyllo med extra strength men förminskad intelligens.
