"use strict";
import Creature from "./creatures/creature";
import Humanoid from "./creatures/humanoids/humanoid";
import Lizardman from "./creatures/humanoids/lizardmen/lizardman";
import Dwarf from "./creatures/humanoids/dwarf/dwarf";
import Longbeards from "./creatures/humanoids/dwarf/longbeards";
import Halflings from "./creatures/humanoids/halflings/halfling";
import Human from "./creatures/humanoids/human";
import Skaning from "./creatures/humanoids/skåning";
import Knight from "./type/humanoids/knight";
import Strong from "./type/humanoids/strong";
import Weak from "./type/humanoids/weak";
import Stupid from "./type/humanoids/stupid";
import DeathKnight from "./type/humanoids/deathKnight";


interface worldInterface {
  creatures: {[key: string]: any},
  types: {[key: string]: any} 
}

let world: worldInterface = {
  creatures: {
    Human: Human,
    Lizardman: Lizardman,
    Dwarf: Dwarf,
    Longbeards: Longbeards,
    Skåning: Skaning,
    Halflings: Halflings
  },
  types: {
    knight: Knight,
    weak: Weak,
    strong: Strong,
    deadKnight: DeathKnight,
    stupid: Stupid
  }
};

export default world;
