"use strict";
import { t, Text } from "@mimer/text";
import { c } from "@mimer/calculation";
import Action from "../../../../game/action";
import Creature from "../../creature";
import Humanoid from "../humanoid";

export default class Halfling extends Humanoid {
    static creators = ["blytinator"];
    static value=300;

    constructor(name:string, type: string[],  creators=Halfling.creators)
    {
        super(name, ["Halvling"].concat(type), creators);
        this.size = c().value(3).add.dice(0,6);
        this.condition = c().value(9).add.dice(0,10);
        this.strength = c().value(3).add.dice(0,4);
        this.flexibility = c().value(8).add.dice(0,11);
        this.intelligent = c().value(12).add.dice(5, 12);
        this.charisma = c().value(9).add.dice(0,14);
        this.wisdom = c().value(7).add.dice(0,10);
        this.might = c().value(3).add.dice(0,7);
        this.maxHP = c(this).value(40).add.property("wisdom").add.property("condition");
        this.hp = NaN;
        this.cost += 300;
        
        this._description = t("En halv människa, inte så stark eller stor men mycket smart och smidig.");
        this._salute = t("Jag kan kanske inte slåss så bra men jag kan tänka till");
        this._ask = t("Hur ska jag göra nu?")
        this._lose = t("Jag behöver kanske tänka om min strategi...");
        this._win = t("Ytterliggare en vinst!")
        this._title = t();
    }


    magicTrick(): Action {
        const my_action = new Action(this, "Utför magitrick", "Magiskt förbluffad");
        my_action.tags.push("body");
        my_action.damage.property("intelligent").add.dice(0,10);
        my_action.initiativ.dice(1,10).add.property("condition")
        my_action.accuracy.value(0.5).add.dice(0,0.5).percent.value(70);
        my_action.description = t("Ett trolleritrick som förbluffar fienden");
        return my_action;
    }

    selection():Action[] {
        let result = super.selection();
        result.push(this.magicTrick());
        this.numberOfSelections++;
        return result;
    }

    static infoChooseMe() {
        return t("Välj en halvling");
    }

    static info() {
        return t("En vanlig halvling").blue;
    }
}