"use strict";
import { t, Text } from "@mimer/text";
import { c } from "@mimer/calculation";
import Action from "../../../game/action";
import Humanoid from "../../creatures/humanoids/humanoid";

// timce

export default function DeadKnight(base: any) {
  return class DeadKnight extends base {
    static creators = ["timce"];
    static value = 500;

    constructor(...args: any[]) {
      super(args[0] as string);
      this.cost += DeadKnight.value;
      this._description = t("En odöd krigare ");
      this._salute = t("Ni kommer alla gå med mig förr eller senare");
      this._lose = t("Jag kommer att återvända!");
      this._ask = t("Omae wa mou shindeiru?");
      this._win = t("Den Odöda armen växer!");
      this._title = t("Odöd Riddare");
    }

    cut() {
      const my_action = new Action(this, "Svärdshugg", "huggen med svärd");
      my_action.tags.push("steel");
      my_action.damage
        .dice(1, 10)
        .add.value(20)
        .mult.property("strength");
      my_action.initiativ.property("flexibility").add.value(2);
      my_action.accuracy.value(1).percent.value(90);
      my_action.description = t("Ett knytnävesslag");
      return my_action;
    }

    selection(): Action[] {
      let result = super.selection();
      result.push(this.cut());
      this.numberOfSelections++;
      return result;
    }

    damage(action: Action): Action {
      let result = super.damage(action);
      result.damage.sub.value(20);
      return result;
    }

    static info(): Text {
      return t("En Odöd Death Knight.").bold.blue;
    }
  };
}
