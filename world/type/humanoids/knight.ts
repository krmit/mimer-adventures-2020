"use strict";
import { t, Text } from "@mimer/text";
import { c } from "@mimer/calculation";
import Action from "../../../game/action";
import Humanoid from "../../creatures/humanoids/humanoid";

export default function Knight(base: any) {
  return class Knight extends base {
    static creators = ["timce"];
    static value = 500;

    constructor(...args: any[]) {
      super(args[0] as string);
      this.cost += Knight.value;
      this._description = t(
        "Riddaren är en mäktig krigare. ",
        "Ridaren gör stor skada genom att hugga med sitt ",
        "svärd. Rustningen skyddar mot olika attacker."
      );
      this._salute = t("Jag ska rädda dig ifrån detta hemska monster! 😇");
      this._lose = t("Ack och ve, onskan vinner igen i denna onda värld!");
      this._win = t("Rättvisan har segrat!");
      this._title = t("Riddare");
    }

    cut() {
      const my_action = new Action(this, "Svärdshugg", "huggen med svärd");
      my_action.tags.push("steel");
      my_action.damage
        .dice(1, 10)
        .add.value(2)
        .mult.property("strength");
      my_action.initiativ.property("flexibility").add.value(2);
      my_action.accuracy.value(1).percent.value(90);
      my_action.description = t("Ett knytnävesslag");
      return my_action;
    }

    selection(): Action[] {
      let result = super.selection();
      result.push(this.cut());
      this.numberOfSelections++;
      return result;
    }

    damage(action: Action): Action {
      let result = super.damage(action);
      result.damage.sub.value(10);
      return result;
    }

    static info(): Text {
      return t("En bra krigare!").bold.blue;
    }
  };
}
