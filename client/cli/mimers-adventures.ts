"use strict";
import { t, Text, JSONToText, TextToTerminal } from "@mimer/text";
import promptly from "promptly";
import fetch from "node-fetch";

const text_server:URL = new URL("http://localhost:3000");
//const text_server:URL = new URL("https://jsonplaceholder.typicode.com/post");

let answer:string|number="start";

async function REPL(url:URL): Promise<void> {
  const token = await fetch(url.toString()).then(res => res.text());
  while(true) {
    const data_to_send =  {token:token, prompt:answer};
    const post_command = { method: 'POST', body:JSON.stringify(data_to_send ), headers: { 'Content-Type': 'application/json' }};
  let from_server = await fetch(url.toString(), post_command).then(res => res.json());
  let text:Text = JSONToText(from_server);
  console.log(TextToTerminal(text));
  answer = await promptly.prompt(">", {default:"", retry: false});
  if(answer === "q") {
     break;
  }
  }
  return;
}

REPL(text_server);
