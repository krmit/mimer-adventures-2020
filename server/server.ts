"use strict";
import express from "express";
import { Request, Response } from 'express';
import Game from "../game/game";
import QuickGame from "../game/quickGame";
import Player from "../game/player";
import { t, Text, TextToJSON, TextToTerminal } from "@mimer/text";
import { v4 as uuid } from 'uuid';

/**
 * The  Server class creating a game server.
 */

 export type gameID = number;
 export type playerID = number;
 export type userID = string;

 export interface gameConfig {
   player:playerID;
   game:gameID;
 }
 export interface gameComand{
  type:string;
}

class User {
  name:string;
  games:gameConfig[]=[];

  constructor (name: string) {
     this.name = name;
  }
}

const UserModel:  {[key: string]: User} = {};
const GameModel: Game[] = [];

const router = express();

router.use(express.json());

router.get('/',  (req: Request, res:Response, next:any) => {
  const id = uuid();
  res.send(id);
  res.locals["command"]="Start connection";
  res.locals["command-value"]=id;
  next();
})

router.post('/',  (req: Request, res:Response, next:any) => {
  const token = req.body["token"];
  const msg = t();
  if(UserModel[token]) {
    const user = UserModel[token];
    const config = user.games[0];
    const game = GameModel[config.game];
    const comand = req.body;
    msg.add(game.next(config.player, comand));
    res.locals["command"]="Do"; 
    res.locals["command-value"]=UserModel[token].name;
  } else if(req.body["prompt"] !== "start") {
     const user = new User(req.body["prompt"]);
     UserModel[token] = user;
    msg.add(t("🔥 Välkommen "+req.body["prompt"]+"! 🔥"));
    /* Create Quick game*/
    const game = new QuickGame("Test Game");
    GameModel.push(game);
    const player = new Player(user.name);
    const player_id = game.addPlayer(player);
    let config = {player: player_id, game: GameModel.length-1}
    user.games.unshift(config);
    game.start();  

    /* End game creation */
    res.locals["command"]="Login";
    res.locals["command-value"]=UserModel[token].name; 
   } else {
    msg.add(t('🔥 Startar spel Mimer Äventyr 🔥 🤠\n').newline);
    msg.add(t('❓ Vad är namnet på spelaren?'));
    res.locals["command"]="Login";
   }
  res.json(TextToJSON(msg));
  next();
})

router.use((req: Request, res:Response, next:any) => {
  const datetime = new Date();
  let msg = t("[",datetime.getHours(), ":", datetime.getMinutes(), ":", datetime.getSeconds(),"]").green;
  msg.add(" ")
  .add(t(req.body["prompt"]?req.body["prompt"]:"").red).add(" # ")
  .add(res.locals["command"]?res.locals["command"]:"").add(" # ")
  .add(t(res.locals["command-value"]?res.locals["command-value"]:"").bold.yellow);
  console.log(TextToTerminal(msg));
  next();
});

const port = 3000;
router.listen(port, () => console.log(TextToTerminal(t("Test ",port).bold.blue)));