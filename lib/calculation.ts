"use strict";
import { t, Text } from "./text";

/**
 * Shorhand function to easy create a Calculation object.
 *
 * @params param Is interpreted by its type.
 * @return A new Calculation that can be modified by the methods of Calculation.
 */

export function c(param: any = undefined) {
  return new Calculation(param);
}

// From https://stackoverflow.com/a/47593316
// We can not use Math.randome since we need tha ability to seed.
function xmur3(str: string) {
  for (var i = 0, h = 1779033703 ^ str.length; i < str.length; i++)
    (h = Math.imul(h ^ str.charCodeAt(i), 3432918353)),
      (h = (h << 13) | (h >>> 19));
  return function() {
    h = Math.imul(h ^ (h >>> 16), 2246822507);
    h = Math.imul(h ^ (h >>> 13), 3266489909);
    return (h ^= h >>> 16) >>> 0;
  };
}

let random = xmur3("mimer");

/**
 * The Calculation class represent calculation.
 */
export class Calculation {
  private expresions: Expresion[];
  private data: any;
  private static seed = "";
  private static counter = 0;

  /**
   * Create a Calculation from data
   *
   * @param obj An object that will be used as data.
   */

  constructor(obj: any = undefined) {
    this.data = obj;
    this.expresions = [];
  }

  /**
   * Create a dice in the calculation. It will by random choose value between a start value and an end value.
   *
   * @params param If the only param it will be the end-value, in other case it will be the start value.
   * @end param The end value.
   * @return The modified Calculation that can be futher modified by the methods of Calculation.
   */
  dice(param: number, end: number) {
    this.expresions.push(new Expresion("dice", param, [end]));
    return this;
  }

  get add() {
    this.expresions.push(new Expresion("add"));
    return this;
  }

  get sub() {
    this.expresions.push(new Expresion("sub"));
    return this;
  }

  get div() {
    this.expresions.push(new Expresion("div"));
    return this;
  }

  get mult() {
    this.expresions.push(new Expresion("mult"));
    return this;
  }

  get percent() {
    this.expresions.push(new Expresion("percent"));
    return this;
  }

  value(param: number) {
    this.expresions.push(new Expresion("value", param));
    return this;
  }

  property(...names: string[]) {
    this.expresions.push(new Expresion("property", this.data, names));
    return this;
  }

  static setSeed(seed: string = "") {
    // All rolls of any Calculation will use this seed after thid method is called.
    Calculation.seed = seed;
  }

  roll(seed: string = "") {
    if (seed !== "") {
      random = xmur3(seed);
    } else if (Calculation.seed !== "") {
      random = xmur3(Calculation.seed + Calculation.counter * 17);
      Calculation.counter++;
    } else {
      random = xmur3(
        Math.random()
          .toString(36)
          .substring(2, 12)
      );
    }
    for (let e of this.expresions) {
      e.roll();
    }
    return this;
  }

  unroll() {
    for (let e of this.expresions) {
      e.unroll();
    }
    return this;
  }

  get showRange(): Text {
    let min = this.min;
    let max = this.max;

    if (min < max) {
      return t(
        t(String(min)).green.fixed(0),
        "-",
        t(String(max)).green.fixed(0)
      );
    } else {
      return t(String(min)).green;
    }
  }

  get toMimerText(): Text {
    let result = new Text();
    for (let e of this.expresions) {
      result.add(e.toMimerMsg);
    }

    const calculation_result = this.result;
    if (!isNaN(calculation_result)) {
      result.add(t("=").bold).add(t(String(calculation_result)).blue);
    }

    return result;
  }

  get result(): number {
    let result_text = "";
    for (let e of this.expresions) {
      if (e.value === NaN) {
        return NaN;
      }
      result_text += e.value;
    }

    return eval(result_text);
  }

  get max(): number {
    let result_text = "";
    for (let e of this.expresions) {
      if (e.max === NaN) {
        return NaN;
      }
      result_text += e.max;
    }

    return eval(result_text);
  }

  get min(): number {
    let result_text = "";
    for (let e of this.expresions) {
      if (e.min === NaN) {
        return NaN;
      }
      result_text += e.min;
    }

    return eval(result_text);
  }
  get toText(): string {
    return this.toMimerText.toText;
  }

  get toJSON(): any {
    let result: any = {};
    result.calculation = this.toMimerText.toJSON;
    if (!Number.isNaN(this.result)) {
      result.result = this.result;
    }
    return result;
  }

  get toTerminal(): string {
    return this.toMimerText.toTerminal;
  }
}

class Expresion {
  private operator: string;
  private param: any;
  private params: (number | string)[];
  private _value: number;
  private _opt: string;

  constructor(
    operator: string,
    param: any = undefined,
    params: (number | string)[] = []
  ) {
    this.operator = operator;
    this.param = param;
    this.params = params;
    this._opt = "";
    this._value = NaN;

    switch (operator) {
      case "value":
        this._value = param;
        break;
      case "add":
        this._opt = "+";
        this._value = 0;
        break;
      case "sub":
        this._opt = "-";
        this._value = 0;
        break;
      case "mult":
        this._opt = "*";
        this._value = 0;
        break;
      case "div":
        this._opt = "/";
        this._value = 0;
        break;
      case "percent":
        this._opt = "*";
        this._value = 0;
        break;
    }
  }

  roll() {
    if (this.operator === "dice") {
      //  (2 ^ 32) - 1 = 4294967295
      this._value =
        Math.floor(
          (random() / 4294967295) *
            ((this.params[0] as number) - this.param + 1)
        ) + this.param;
    }
  }

  unroll() {
    if (this.operator === "dice") {
      this._value = NaN;
    }
  }

  calculation(type = "value") {
    if (this.operator === "property") {
      let result = this.param;

      for (let p of this.params) {
        result = result[p];
        if (result instanceof Calculation) {
          result = result.result;
        }
      }
      return result;
    } else if (this.operator === "percent") {
      return "*1/100*";
    } else if (this._opt !== "") {
      return this._opt;
    } else {
      if (type === "max") {
        return this.params[0];
      } else if (type === "min") {
        return this.param;
      } else {
        return this._value;
      }
    }
  }

  get value(): number | string {
    return this.calculation("value");
  }

  get max(): number | string {
    return this.calculation("max");
  }

  get min(): number | string {
    return this.calculation("min");
  }

  get toMimerMsg(): Text | string {
    switch (this.operator) {
      case "value":
        return t(String(this.param)).blue;
        break;
      case "add":
      case "sub":
      case "mult":
      case "div":
        return t(this._opt as string).red;
        break;
      case "percent":
        return t("*%").red;
        break;
      case "property":
        let path_text = this.params.reduce((a: any, b: any) => a + "." + b);
        return t(t(path_text + ":").green, t(String(this.value)).blue);
        break;
      case "dice":
        if (isNaN(this._value)) {
          return t(
            "[",
            t(String(this.param)).blue,
            t("–").bold,
            t(String(this.params[0])).blue,
            "]"
          );
        } else {
          return t(
            "[",
            t(String(this.param)).green,
            t("–").bold,
            t(String(this.params[0])).green,
            t(":").bold,
            t(String(this._value)).blue,
            "]"
          );
        }
        break;
      default:
        return String(this._value);
    }
  }
}
