"use strict";
import { t, Text, TextToJSON, JSONToText } from "@mimer/text";
import { c, Calculation } from "@mimer/calculation";
import Player from "./player";
import Game from "./game";
import Mode from "./mode";
import Select from "./select";
import Battel from "./battel";

import world from "../world/world";
import Creature from "../world/creatures/creature";


/**
 * The  Game class creting a game engine for logic.
 */

type statusType = "start"|"friend"|"enemy"|"battel"

export default class QuickGame extends Game {
  mode?:Mode;
  status:statusType="start";

  constructor(name = "") {
    super(name);
  }

  start() {
    this.status = "friend";
    this.mode = new Select();
    (this.mode as Select).setNameOnCreatue("Adam");
  }

  next(id:number,command:{[key: string]: number|string}):Text {
    let result = t();
    if(this.mode) {
      if(this.status !== "battel") {
         result.add(this.mode.move(command,""));
         result.add(t("\n"));
         result.add(this.mode.endTurn());
      } else {
          result.add(this.mode.move(command,"one"));
          result.add(t("\n"));
          result.add(this.mode.move({"prompt":1},"two"));
          result.add(this.mode.endTurn());
      }
      if(!this.mode.isMode()) {
        switch(this.status) {
          case "friend":
             const creature:Creature = this.mode.endMode();
             creature.roll();
             this.players[0].team.push(creature);
             result.add(creature.showStats());
             result.add(t().newline);
             result.add(creature.saySalute);
             result.add(t().newline);
             this.mode = new Select();
             (this.mode as Select).setNameOnCreatue("Bertil");
             this.status="enemy";
          break;
          case "enemy":
             const player = new Player("AI random");
             this.addPlayer(player);
             const enemy:Creature = this.mode.endMode();
             enemy.roll();
             this.players[1].team.push(enemy);
             result.add(enemy.showStats());
             result.add(t().newline);
             result.add(enemy.saySalute);
             result.add(t().newline);
             this.mode = new Battel();
             (this.mode as Battel).addPlayer("one", this.players[0]);
             (this.mode as Battel).addPlayer("two", this.players[1]);
             this.status="battel";
          break;
          case "battel":
             result.add(t("\nEnd of battel\n").bold.red);
             this.status="battel";
          break;
        }
      }
      return result;
    } else {
      return t("Error: No mode choosen").red;
    }
  }

}