"use strict";
import { t, Text } from "@mimer/text";
import { c, Calculation } from "@mimer/calculation";

import Action from "../game/action";
import world from "../world/world";
import Creature from "../world/creatures/creature";


export default abstract class Mode {
  name = "";
  msgTurn:Text =t();
  turn = 0;

  constructor(name = "") {
    this.name = name;
  }

  abstract move(command:{[key: string]: number|string}, playerId:string):Text 

  endTurn() {
    this.turn++;
    return this.msgTurn;
  }

  abstract isMode():boolean;

  abstract endMode():any;
}
