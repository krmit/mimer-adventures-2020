"use strict";
import { t, Text } from "@mimer/text";
import { c, Calculation } from "@mimer/calculation";
import Creature from "../world/creatures/creature";

/**
 * The  Action class represent action.
 */

interface Occasions {
  all?: Action;
  allEnemy?: Action;
  allFriend?: Action;
}

export default class Action {
  name: string;
  verb: string;
  tags: string[] = [];
  description = t("");
  data: any;
  by: Creature;
  byPlayer?: string;
  byId?: number;
  target?: Creature;
  targetPlayer?: string;
  targetId?: number;
  initiativ: Calculation;
  accuracy: Calculation;
  damage: Calculation;
  heal: Calculation;
  occasions: Occasions = {};

  constructor(by: any, name = "", verb = "") {
    this.name = name;
    this.verb = verb;
    this.by = by;
    this.data = Object.assign({}, this.by);
    this.initiativ = c(this.data);
    this.accuracy = c(this.data);
    this.damage = c(this.data);
    this.heal = c(this.data);
  }

  setTarget(target: Creature) {
    this.target = target;
    this.data["target"] = target;
  }

  setPlayer(name: string, id: number) {
    this.byPlayer = name;
    this.byId = id;
  }

  setTargetPlayer(name: string, id: number) {
    this.targetPlayer = name;
    this.targetId = id;
  }

  get showName(): Text {
    if (this.name !== "") {
      return t(this.name);
    } else {
      return t("Aktion");
    }
  }

  get showLabel(): Text {
    return t(this.showName.green, " > ").span(24);
  }

  get showDamageLabel(): Text {
    return t(
      this.target!.labelName,
      " blir ",
      this.verb,
      " av ",
      this.by.labelName,
      ": "
    ).span(64);
  }

  roll() {
    this.initiativ.roll();
    this.accuracy.roll();
    this.damage.roll();
    this.heal.roll();
  }

  get showTitel(): Text {
    return this.showName.headline;
  }

  get showInfo(): Text {
    return t(
      this.showLabel,
      "⚔️ : ",
      t(this.damage.showRange).span(6),
      "🎯: ",
      t(this.accuracy.result).percent,
      " ",
      "⏳: ",
      this.initiativ.showRange
    );
  }

  get showDescription(): Text {
    return t(
      this.showTitel.green.newline,
      t().newline,
      this.description.newline,
      t().newline,
      t("⚔️ : ", this.damage.toMimerText).newline,
      t("🎯: ", this.accuracy.toMimerText).percent.newline,
      t("⏳: ", this.initiativ.toMimerText).newline
    );
  }
}
