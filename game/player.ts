"use strict";
import { t, Text } from "@mimer/text";
import { c, Calculation } from "@mimer/calculation";

import Action from "../game/action";
import world from "../world/world";
import Creature from "../world/creatures/creature";

export function parseCreature(pathCreature: string) {
  const creature_class_list = pathCreature.split("/");

  let result: Creature;
  result = world.creatures[creature_class_list[0]];
  for (let i = 1; i < creature_class_list.length; i++) {
    result = world.types[creature_class_list[i]](result);
  }
  return result;
}

export default class Player {
  name = "";
  team:Creature[]=[];

  constructor(name = "") {
    this.name = name;
  }

   addCreature(creature:Creature) {
     this.team.push(creature);
   }
}
