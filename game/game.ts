"use strict";
import { t, Text } from "@mimer/text";
import { c, Calculation } from "@mimer/calculation";
import Player from "./player";

import Action from "../game/action";
import world from "../world/world";
import Creature from "../world/creatures/creature";

/**
 * The  Game class creting a game engine for logic.
 */


export default abstract class Game {
  name: string;
  players:  Player[] = [];
//  status: "ready"|"active"|"ended" = "ready";

  constructor(name = "") {
    this.name = name;
  }

  addPlayer(player: Player) {
    this.players.push(player);
    return this.players.length-1;
  }

  abstract start(team: string):void

  abstract next(id:number,command:{[key: string]: number|string}):Text 
}