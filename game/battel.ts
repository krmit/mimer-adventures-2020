"use strict";
import { t, Text } from "@mimer/text";
import { c, Calculation } from "@mimer/calculation";
import Player from "./player";
import Action from "../game/action";
import Mode from "./mode";

import world from "../world/world";
import Creature from "../world/creatures/creature";

function values(o:any) {
  return Object.keys(o).map(key => o[key]);
}

type statusType = "start"|"active"|"prolog"|"end"

/**
 * The  Game class creting a game engine for logic.
 */
export default class Battel extends Mode {
  status:statusType="start";
  players:{[key: string]: Player}={};
  actions: Action[] = [];

  constructor(name = "") {
    super(name);
  }

  addPlayer(id:string, player:Player) {
       this.players[id]=player;
       t("hej ", t("world").green).underline;
    }

  move(command:{prompt: number}, playerId:string) {
    let result = t();
    switch(this.status) {
    case "start":
      this.status = "active";
      this.msgTurn = t("Battel begin!").bold.headline;
    break;
    case "active":
      this.status = "prolog";
      this.msgTurn = t(t("Turn: ", this.turn).bold.headline, t("\n").newline);
      let action = this.players[playerId].team[0].selection()[command.prompt];
    break;
    case "prolog":
      this.status = "end";
      this.msgTurn = t("Battel end!").headline;
    break;
    }
    return result;
  }
  
  endTurn() {
    let result = t();
    this.actions = [];
    result.add(super.endTurn());
    result.add(
      t(...this.players["one"].team[0].selection().map((item: Action) => item.showInfo))
        .orderedList
    ).newline;
    return result;
  }

  isMode() {
    return this.status !== "end";
  }

  endMode() {
    return "";
  }
}