"use strict";
import { t, Text } from "@mimer/text";
import { c, Calculation } from "@mimer/calculation";
import Player from "./player";
import Mode from "./mode";

import world from "../world/world";
import Creature from "../world/creatures/creature";

function values(o:any) {
  return Object.keys(o).map(key => o[key]);
}

type statusType = "creature"|"type"|"create"|"end"

/**
 * The  Game class creting a game engine for logic.
 */
export default class Select extends Mode {
  status:statusType="creature";
  msgTurn:Text =t();
  creature?:any; // class of creature
  creatureName:string = "Adam";

  constructor(name = "") {
    super(name);
  }

  setNameOnCreatue(name:string) {
    this.creatureName = name; 
  }

  move(command:{prompt: number}) {
    let result = t();

    console.log(this.status);
    switch(this.status) {
    case "creature":
      for(const creature of values(world.creatures)) {
         result.add(creature.infoChooseMe().newline);
      }
      this.status = "type";
      this.msgTurn = t("Vilken varelse vill du välja?").green
    break;
    case "type":
      this.creature = values(world.creatures)[command.prompt-1];
      for(const type of values(world.types)) {
         result.add(type(this.creature).info());
      }
      this.status = "create";
      this.msgTurn = t("Vilken type vill du välja?").green
    break;
    case "create":
      this.creature = values(world.types)[command.prompt-1](this.creature);
      this.status = "end";
      this.msgTurn = t("Creating a character").green.add("\n");
    break;
    }

    return result.orderedList;
  }

  endTurn() {
    return this.msgTurn;
  }

  isMode() {
    return this.status !== "end";
  }

  endMode() {
    return new this.creature(this.creatureName);
  }
}