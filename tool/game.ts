"use strict";
import { t, Text } from "@mimer/text";
import { c, Calculation } from "@mimer/calculation";
import Logger from "../lib/logger";

import Action from "../game/action";
import world from "../world/world";
import Creature from "../world/creatures/creature";

const log = new Logger().log(4);
/**
 * The  Game class creting a game engine for logic.
 */

export function parseCreature(pathCreature: string) {
  const creature_class_list = pathCreature.split("/");
  log.debug(creature_class_list);
  let result: Creature;
  result = world.creatures[creature_class_list[0]];
  for (let i = 1; i < creature_class_list.length; i++) {
    result = world.types[creature_class_list[i]](result);
  }
  return result;
}

export class Game {
  name: string;
  data: any = {};
  reports: any = {};
  turnActions: Action[] = [];
  mode = "";
  phase = "";
  turn = 0;

  constructor(name = "") {
    this.name = name;
  }

  setMode(mode: string) {
    this.mode = mode;
  }

  setPhase(phase: string) {
    this.phase = phase;
  }

  addPlayer(team: string) {
    this.data[team] = [];
    return 
  }

  createRandomCharater(
    team: string,
    name: string,
    pathCreature: string
  ): number {
    let character_class: any = parseCreature(pathCreature);
    let character = new character_class(name);
    character.roll();
    this.addCharater(team, character);
    return character;
  }

  addCharater(team: string, charater: Creature): number {
    return this.data[team].push(charater) - 1;
  }

  gameStart(): Text {
    let status_reports = t();
    for (let player_name of Object.keys(this.data)) {
      status_reports.add(t(player_name).headline);
      for (let character of this.data[player_name]) {
        status_reports.add(character.showStats());
        status_reports.add(t().newline);
        status_reports.add(character.saySalute);
        status_reports.add(t().newline);
      }
    }
    status_reports.add(this.battelStart());
    return status_reports;
  }

  battelStart(): Text {
    this.turnStart();
    this.turn = 0;
    return t("Battel begin!").bold.headline;
  }

  battelEnd(): Text {
    return t("Battel end!").headline;
  }

  turnStart(): Text {
    this.turn++;
    this.reports = {};
    this.turnActions = [];
    for (let key of Object.keys(this.data)) {
      this.reports[key] = t(t("Turn: ", this.turn).bold.headline, t().newline);
    }
    return t();
  }

  turnCommand(playerName: string, command: any): Text {
    let action = this.data[playerName][command.characterId].selection()[
      command.selection
    ];
    action.setPlayer(playerName, command.characterId);
    let target = this.data[command.targetPlayer][command.targetId];
    action.setTarget(target);
    let damage = target.damage(action);
    damage.setTargetPlayer(command.targetPlayer, command.targetId);
    this.turnActions.push(damage);
    return t();
  }

  turnEnd(): any {
    // This is not finish, need one report for each charater and common reports.
    // Do action and report
    let action_list = this.turnActions;
    action_list.sort((a: any, b: any) => {
      if (a.initiativ.result > b.initiativ.result) {
        return -1;
      }
      if (a.initiativ.result < b.initiativ.result) {
        return 1;
      }
      return 0;
    });

    for (let action of action_list) {
      if (action.by.isFighting() && action.target!.isFighting()) {
        let by_player = action.byPlayer!;
        let target_player = action.targetPlayer!;
        let character_id = action.targetId!;
        let damage_report = this.data[target_player][character_id].do(action);
        this.reports[target_player].add(
          t(t(target_player, "> ").red.bold, damage_report)
        );
        this.reports[by_player].add(
          t(t(target_player, "> ").bold, damage_report)
        );
      }
    }

    // Do player and charater report and examen status.
    let status_reports = t();
    for (let player_name of Object.keys(this.data)) {
      status_reports.add(t(player_name).headline);
      for (let character of this.data[player_name]) {
        status_reports.add(character.showStatus);
      }
      status_reports.add(t().newline);
    }

    // Do player and charater report and examen status.
    for (let player_name of Object.keys(this.data)) {
      this.reports[player_name].add(status_reports);
      this.reports[player_name].add(t("Ditt val!").headline);
      let game_over = true;
      for (let character of this.data[player_name]) {
        if (character.isFighting()) {
          game_over = false;
          this.reports[player_name].add(character.showBanner);
          this.reports[player_name].add(
            t(...character.selection().map((item: Action) => item.showInfo))
              .orderedList
          ).newline;
          this.reports[player_name].add(t().newline);
          this.reports[player_name].add(character.sayQuestion);
        }
        if (game_over) {
          this.reports[player_name].newline
            .add(t("Game Over!").red.headline)
            .addTag("GameOver");
        }
      }
    }

    return this.reports;
  }

  // API for Server

  phaseEnd(): Text {
    let result = this.turnEnd();
    this.turnStart();
    return result;
  }

  phaseCommand(playerName: string, command: any): Text {
    let result = this.turnCommand(playerName, command);
    return result;
  }
}
