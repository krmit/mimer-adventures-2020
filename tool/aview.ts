"use strict";
import { t, Text, TextToJSON, TextToTerminal } from "@mimer/text";
import { c, Calculation } from "@mimer/calculation";
import { parseCreature, Game } from "./game";
import Action from "../game/action";
import world from "../world/world";
import Creature from "../world/creatures/creature";
import Logger from "../lib/logger";
import yargs from "yargs";

const logger = new Logger();

const args = yargs
  .command("info [pathCreature]", "start the server", y => {
    y.positional("pathCreature", {
      describe: "class and mixins",
      default: "Human"
    });
  })
  .command("roll [pathCreature]", "start the server", y => {
    y.positional("pathCreature", {
      describe: "class and mixins",
      default: "Human"
    });
  })
  .command("stats [pathCreature]", "start the server", y => {
    y.positional("pathCreature", {
      describe: "class and mixins",
      default: "Human"
    });
  })
  .command("calculation [pathCreature]", "start the server", y => {
    y.positional("pathCreature", {
      describe: "class and mixins",
      default: "Human"
    });
  })
  .command("selections [pathCreature]", "start the server", y => {
    y.positional("pathCreature", {
      describe: "class and mixins",
      default: "Human"
    });
  })
  .command("action [pathCreature] [selection]", "start the server", y => {
    y.positional("pathCreature", {
      describe: "class and mixins",
      default: "Human"
    });
    y.positional("selection", { describe: "class and mixins", default: 0 });
  })
  .command(
    "damage [pathCreature] [selection] [pathCreatureOpponent]",
    "start the server",
    y => {
      y.positional("pathCreature", {
        describe: "class and mixins",
        default: "Human"
      });
      y.positional("pathCreatureOpponent", {
        describe: "class and mixins",
        default: "Human"
      });
      y.positional("selection", { describe: "class and mixins", default: 0 });
    }
  )
  .command(
    "turn [pathCreature] [selection] [pathCreatureOpponent] [selectionOpponent]",
    "start the server",
    y => {
      y.positional("pathCreature", {
        describe: "class and mixins",
        default: "Human"
      });
      y.positional("pathCreatureOpponent", {
        describe: "class and mixins",
        default: "Human"
      });
      y.positional("selection", { describe: "class and mixins", default: 0 });
      y.positional("selectionOpponent", {
        describe: "class and mixins",
        default: 0
      });
    }
  )
  .command(
    "battel [pathCreature] [pathCreatureOpponent]",
    "start the server",
    y => {
      y.positional("pathCreature", {
        describe: "class and mixins",
        default: "Human"
      });
      y.positional("pathCreatureOpponent", {
        describe: "class and mixins",
        default: "Human"
      });
    }
  )
  .command(
    "battelStats [numberOfBattels] [pathCreature] [pathCreatureOpponent]",
    "start the server",
    y => {
      y.positional("pathCreature", {
        describe: "class and mixins",
        default: "Human"
      });
      y.positional("pathCreatureOpponent", {
        describe: "class and mixins",
        default: "Human"
      });
      y.positional("numberOfBattels", {
        describe: "class and mixins",
        default: 10
      });
    }
  );

args
  .describe("s", "Give a text as seed for random numbers")
  .alias("s", "seed")
  .default("s", "");
const argv = logger.yarg(args).argv;
const log = logger.log(argv);
log.trace(argv);

let result: Text;

const command = argv._[0];
log.trace(command);

let character: any;
let opponent: any;
let character_class: any;
let opponent_class: any;
let action: any;
let game: any;
let selection = 0;
let selectionOpponent = 0;
let last_report: any;
let numberOfBattels = 1;

if (argv.pathCreature) {
  character_class = parseCreature(argv.pathCreature);
}
if (argv.pathCreatureOpponent) {
  opponent_class = parseCreature(argv.pathCreatureOpponent);
}
if (argv.selection) {
  selection = Number(argv.selection) - 1;
}
if (argv.selectionOpponent) {
  selectionOpponent = Number(argv.selectionOpponent) - 1;
}
if (argv.seed) {
  Calculation.setSeed(argv.seed);
}
if (argv.numberOfBattels) {
  numberOfBattels = argv.numberOfBattels;
}
switch (command) {
  case "info":
    result = character_class.info();
    break;
  case "roll":
    character = new character_class("Adam");
    character.roll();
    result = t("No errors?").green.bold;
    break;
  case "stats":
    character = new character_class("Adam");
    character.roll();
    result = character.showStats();
    break;
  case "calculation":
    character = new character_class("Adam");
    character.roll();
    result = character.showCalculation();
    break;
  case "selections":
    character = new character_class("Adam");
    character.roll();
    result = t(
      character.showBanner,
      t(...character.selection().map((item: Action) => item.showInfo))
        .orderedList
    ).newline;
    //result = character.showCalculation();
    break;
  case "action":
    character = new character_class("Adam");
    character.roll();
    result = character.selection()[selection].showDescription;
    break;
  case "damage":
    character = new character_class("Adam");
    opponent = new opponent_class("Adam");
    character.roll();
    opponent.roll();
    action = character.selection()[selection];
    result = opponent.damage(action).showDescription;
    break;
  case "turn":
    result = t(t("Turn Test for a player 'One'").headline);
    game = new Game();
    game.addPlayer("One");
    game.addPlayer("Two");
    game.createRandomCharater("One", "Adam", argv.pathCreature);
    game.createRandomCharater("Two", "Bertil", argv.pathCreatureOpponent);
    game.turnStart();
    game.turnCommand("One", {
      characterId: 0,
      selection: selection,
      targetPlayer: "Two",
      targetId: 0
    });
    game.turnCommand("Two", {
      characterId: 0,
      selection: selectionOpponent,
      targetPlayer: "One",
      targetId: 0
    });
    result.add(game.turnEnd()["One"]);
    break;
  case "battel":
    result = t(t("Game Test for a player 'One'").headline);
    game = new Game();
    game.addPlayer("One");
    game.addPlayer("Two");
    let player_one_charater = game.createRandomCharater(
      "One",
      "Adam",
      argv.pathCreature
    );
    let player_two_charater = game.createRandomCharater(
      "Two",
      "Bertil",
      argv.pathCreatureOpponent
    );
    game.setMode("Battel");
    game.setPhase("Turn");
    result.add(game.gameStart());
    last_report = { One: t(), Two: t() };
    while (
      !last_report["One"].hasTag("GameOver") &&
      !last_report["Two"].hasTag("GameOver")
    ) {
      let player_one_selection =
        Math.floor(Math.random() * player_one_charater.numberOfSelections) + 1;
      let player_two_selection =
        Math.floor(Math.random() * player_two_charater.numberOfSelections) + 1;
      game.turnCommand("One", {
        characterId: 0,
        selection: player_one_selection - 1,
        targetPlayer: "Two",
        targetId: 0
      });
      game.turnCommand("Two", {
        characterId: 0,
        selection: player_two_selection - 1,
        targetPlayer: "One",
        targetId: 0
      });
      last_report = game.phaseEnd();
      result.add(last_report["One"]);
    }
    if (!last_report["One"].hasTag("GameOver")) {
      result.add(t("Spelare 'One' vann 😀").bold.green);
    } else {
      result.add(t("Spelare 'One' förlorade 🙁").bold.red);
    }
    break;
  case "battelStats":
    result = t(t("Game Stats for a player 'One'").headline);
    let win = 0;
    let lost = 0;
    let turn_sum = 0;
    let turn_max = 0;
    let turn_min = 10000;
    console.log("\nBattels:");
    for (let i = 0; i < numberOfBattels; i++) {
      game = new Game();
      game.addPlayer("One");
      game.addPlayer("Two");
      let player_one_charater = game.createRandomCharater(
        "One",
        "Adam",
        argv.pathCreature
      );
      let player_two_charater = game.createRandomCharater(
        "Two",
        "Bertil",
        argv.pathCreatureOpponent
      );
      game.setMode("Battel");
      game.setPhase("Turn");
      game.gameStart();
      last_report = { One: t(), Two: t() };
      while (
        !last_report["One"].hasTag("GameOver") &&
        !last_report["Two"].hasTag("GameOver")
      ) {
        let player_one_selection =
          Math.floor(Math.random() * player_one_charater.numberOfSelections) +
          1;
        let player_two_selection =
          Math.floor(Math.random() * player_two_charater.numberOfSelections) +
          1;
        game.turnCommand("One", {
          characterId: 0,
          selection: player_one_selection - 1,
          targetPlayer: "Two",
          targetId: 0
        });
        game.turnCommand("Two", {
          characterId: 0,
          selection: player_two_selection - 1,
          targetPlayer: "One",
          targetId: 0
        });
        last_report = game.phaseEnd();
      }
      if (i % 64 === 0) {
        process.stdout.write("\n");
      }
      if (!last_report["One"].hasTag("GameOver")) {
        win++;
        process.stdout.write("😀");
      } else {
        lost++;
        process.stdout.write("🙁");
      }

      turn_sum += game.turn;
      if (game.turn < turn_min) {
        turn_min = game.turn;
      }
      if (turn_max < game.turn) {
        turn_max = game.turn;
      }
    }
    result = t(
      t().newline,
      t().newline,
      t("Stats: ").span(16),
      t(win).green.bold,
      t("/").bold,
      t(win + lost).bold,
      "   ",
      t(t((win / numberOfBattels) * 100).fixed(2), "%").bold,
      t().newline
    );
    result.add(
      t(
        t("Antal rundor: "),
        turn_min,
        " < ",
        t(turn_sum / numberOfBattels).fixed(2),
        " < ",
        turn_max
      ).bold
    );
    break;
  default:
    result = t("Bad comand").red.bold;
}

if (false) {
  console.log(TextToJSON(result));
}

if (true) {
  console.log(TextToTerminal(result));
}
